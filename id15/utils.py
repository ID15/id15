import time, sys
from bliss import current_session
import matplotlib.pyplot as plt
import numpy as np
#from bliss.config.static import get_config
#from bliss.common.greenlet_utils import Timeout

from bliss.shell.iter_common import iter_axes_position_all
from bliss.shell.standard import wm

ECONST=12.3984


def wmg(group):
    m=[]
    for axis in iter_axes_position_all():
        if group in axis.axis_name[:len(group)]:
            m.append(axis.axis_name)
    wm(*m)


def get_motor_position(axname):
    with current_session.temporary_config as cfg:
        out = cfg.get(str(axname)).position
    return out


def get_motor(axname):
    with current_session.temporary_config as cfg:
        out = cfg.get(str(axname))
    return out


def safe_retry(func):
    def cleanup_func():
        pass
    def wrapper(maxtries=4, *args,**kwargs):
        for i in range(maxtries):
            try:
                print("Call the function")
                func(*args,**kwargs)
            except KeyboardInterrupt:
                print('Bye')
                cleanup_func()
                break
            except Timeout as e:
                print(f'Timeout with error {e}')
                print(sys.exc_info()[0])
                continue
            except Exception as e:
                print(f'Failed with error {e}')
                print(sys.exc_info()[0])
                continue
            else:
                print("Came home happy.")
                cleanup_func()
                break
            finally:
                # any general cleanup to always be executed
                # will be executed though after each try, whether
                # success, failure or Cntl-C
                print('All things come to an end')
    return wrapper

  
def shopen():
    if current_session.name=='eh3':
        from bliss.setup_globals import sheh3
        sheh3.open()

    
def shclose():
    if current_session.name=='eh3':
        from bliss.setup_globals import sheh3
        sheh3.close()

def qmax(energy=None,x=None,y=None,z=None):
    ZSIZE=1679*0.172
    YSIZE=1475*0.172
    if energy is None:
        energy=get_motor_position('llen')
    if x is None:
        x=get_motor_position('ddx')
    if y is None:
        y=get_motor_position('ddy')-32
    if z is None:
        z=get_motor_position('ddz')
    ymax = max(YSIZE/2-y,YSIZE/2+y)
    zmax = max(ZSIZE/2-z,ZSIZE/2+z)
    r=min(ymax,zmax)
#    print(f'zmax={zmax}; ymax={ymax}')
    tth=np.arctan(r/x)
    q=4*np.pi/ECONST*energy*np.sin(tth/2)
    print(f'Qmax on closest edge = {q:.2f} A')
    
def fscan3d_check():
    from bliss.setup_globals import fscan3d
    p=fscan3d.pars
    
    start1=p.slow1_start_pos
    end1=start1+p.slow1_npoints*p.slow1_step_size
    points1=np.linspace(start1,end1,p.slow1_npoints)
    
    start2=p.slow2_start_pos
    end2=start2+p.slow2_npoints*p.slow2_step_size
    points2=np.linspace(start2,end2,p.slow2_npoints)
    
    startf=p.fast_start_pos
    endf=startf+p.fast_npoints*p.fast_step_size
    pointsf=np.linspace(startf,endf,p.fast_npoints)
    
#    print(points1,points2,pointsf)
    s1,s2=np.meshgrid(points1,points2)
    plt.scatter(s1,s2,marker='+')
    plt.axhline(0,color='k')
    plt.axvline(0,color='k')
    plt.xlabel(p.slow1_motor.name)
    plt.ylabel(p.slow2_motor.name)
    plt.show()
    
    if(endf-startf>360):
        endf=startf+360
    sizes=[p.fast_step_size/360.]*p.fast_npoints
#    print(sizes,len(sizes))
    plt.pie(sizes,startangle=startf,colors=p.fast_npoints*['g'])
    plt.axhline(0,color='k')
    plt.axvline(0,color='k')
    plt.axis('equal')
#    plt.set_title(p.fast_motor.name)
    plt.show()
 

def aeroycontscan_check():
    from bliss.setup_globals import aeroycontscan
    p=aeroycontscan.pars
    
    start1=p.x_start_pos
    end1=start1+p.x_range
    points1=np.linspace(start1,end1,int(p.x_range/p.x_step_size) + 1)
    
    start2=p.y_start_pos
    s_start=start2
    points2=np.zeros([])
    for i,s in enumerate(p.y_step_size_arr):
        n=p.y_npoints_arr[i]
        s_end=s_start+s*n
        points2=points2+np.linspace(s_start,s_end,n)
        s_start=s_end
    end2=s_end
    
#    startf=p.fast_start_pos
#    endf=startf+p.fast_npoints*p.fast_step_size
#    pointsf=np.linspace(startf,endf,p.fast_npoints)
    
#    print(points1,points2,pointsf)
    s1,s2=np.meshgrid(points1,points2)
    plt.scatter(s1,s2,marker='+')
    plt.axhline(0,color='k')
    plt.axvline(0,color='k')
    plt.xlabel('hrrz')
    plt.ylabel('hry')
    plt.show()
    
#    if(endf-startf>360):
#        endf=startf+360
#    sizes=[p.fast_step_size/360.]*p.fast_npoints
##    print(sizes,len(sizes))
#    plt.pie(sizes,startangle=startf,colors=p.fast_npoints*['g'])
#    plt.axhline(0,color='k')
#    plt.axvline(0,color='k')
#    plt.axis('equal')
##    plt.set_title(p.fast_motor.name)
#    plt.show()

   

#def radmap_pilatus(xcen, ycen):
#    dist = get_motor_position('ddx')
#    wvln = 12.3984/get_motor_position('llen')
#    fig, ax = plt.subplots(2,2, figsize=(7,8))
#    ax = np.ravel(ax)
#    rows, cols = np.meshgrid(np.arange(0,1475,1), np.arange(0,1679,1))
#    rows = rows
#    cols = cols
#    pixmap = np.sqrt((rows-ycen)**2+(cols-xcen)**2)
#    rmap = 0.172*pixmap
#    angmap = np.arctan(rmap/dist)
#    qmap = 4*np.pi*np.sin(angmap*0.5)/wvln
#    tthmap = np.degrees(angmap)
#    darr = [pixmap, rmap, qmap, tthmap]
#    for ii in range(len(darr)):
#        pl = ax.imshow(darr[ii], interpolation='none', origin='upper', cmap='viridis')
#        plt.colorbar(pl, ax=ax)
#    plt.tight_layout()
#    plt.show()
    
    
    

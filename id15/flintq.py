import json

from bliss.config.settings import SimpleSetting
from bliss.common.plot import get_flint
from bliss.common.hook import MotionHook

class FlintQCalibration(object):
    def __init__(self, name, config):
        self.name = name
        self.detector_name = config.get("detector_name")
        self.detector_image_size = config.get("detector_image_size", float)
        self.detector_pixel_size = config.get("detector_pixel_size")
        self.detector_calibration = config.get("detector_calibration")

        self.motor_x = config.get("motor_x")
        self.motor_y = config.get("motor_y")
        self.motor_z = config.get("motor_z")
        self.motor_energy = config.get("motor_energy")

        self.__auto_update = SimpleSetting(f"{name}:auto_update", default_value=False)
        self.last_geometry = self.__new_geometry()
        self.last_update = "not initialised"
        self.last_poni = None

    def __new_geometry(self):
        return {'poni1':0, 'poni2':0, 'rot1':0, 'rot2':0, 'rot3':0, 'wavelength':0.1, 'dist':0}

    def __info__(self):
        msg = f"Last geometry updated from : {self.last_update}\n"
        msg += f"Last poni file loaded      : {self.last_poni}\n"
        msg += f"Auto Update with motors    : {self.auto_update}\n"
        msg += f"Last geometry :\n"
        msg += self.to_json()
        return msg

    @property
    def auto_update(self):
        return self.__auto_update.get()

    @auto_update.setter
    def auto_update(self, flag):
        self.__auto_update.set(flag is True)

    def update(self):
        pos_x = self.motor_x.position
        pos_y = self.motor_y.position
        pos_z = self.motor_z.position
        energy = self.motor_energy.position

        geometry = self.__new_geometry()
        geometry["dist"] = pos_x / 1000.
        z = pos_z + self.detector_pixel_size*self.detector_image_size[1]/2
        geometry["poni1"] = z / 1000.
        y = pos_y + self.detector_pixel_size*self.detector_image_size[0]/2
        geometry["poni2"] = (pos_y-32) / 1000.
        geometry["wavelength"] = 12.3984 / energy / 1.0e10
        
        self.last_geometry = geometry
        self.last_update = "motor positions"

        self.update_flint()

    def read_poni(self, ponifile):
        with open(ponifile, "r") as pfile:
            poni = pfile.readlines()

        geometry = self.__new_geometry()
        for line in poni:
            data = line.split(":")
            if data[0] == 'Distance':
                geometry['dist']=float(data[1])
            elif data[0] == 'Poni1':
                geometry['poni1']=float(data[1])
            elif data[0] == 'Poni2':
                geometry['poni2']=float(data[1])
            elif data[0] == 'Rot1':
                geometry['rot1']=float(data[1])
            elif data[0] == 'Rot2':
                geometry['rot2']=float(data[1])
            elif data[0] == 'Rot3':
                geometry['rot3']=float(data[1])
            elif data[0] == 'Wavelength':
                geometry['wavelength']=float(data[1])

        self.last_geometry = geometry
        self.last_update = "poni file"
        self.last_poni = ponifile

        self.update_flint()
    
    def to_json(self): 
        return json.dumps(self.last_geometry,indent=4)

    def update_flint(self):
        flint = get_flint(creation_allowed=False, mandatory=False)
        if flint is None:
            print("WARNING: no flint running. Q calibration not updated")
            return
        plot = flint.get_live_plot(image_detector=self.detector_name)

        geometry = self.last_geometry
        plot.diffraction.set_detector(self.detector_calibration)
        plot.diffraction.set_geometry(**geometry)
        print("Flint qcalibration updated.")

class FlintQCalibrationHook(MotionHook):
    def __init__(self, name, config):
        self.name = name
        super(FlintQCalibrationHook, self).__init__()
        self.calibration_name = config.get("calibration_name")
        self.__calibration = None

    @property
    def calibration(self):
        if self.__calibration is None:
            from bliss import current_session
            try:
                self.__calibration = current_session.config.get(self.calibration_name)
            except:
                self.__calibration = None
        return self.__calibration

    def post_move(self, motion_list):
        if self.calibration:
            if self.calibration.auto_update:
                self.calibration.update()


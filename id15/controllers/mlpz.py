import gevent
import tabulate
from gevent.event import Event

from bliss.controllers.motor import Controller
from bliss.common.axis import AxisState
from bliss.comm.util import get_comm, SERIAL
from bliss.common.utils import object_method


class MLPZMotor(Controller):
    def initialize(self):
        config = self.config.config_dict

        self.comm = get_comm(config, SERIAL)
        self.serial_url = config["serial"]["url"]
        self.pi_version = "not_initialized"

        self._max_nb_move = self.config.get("max_nb_move", int)
        self._pos_deadband = self.config.get("pos_deadband", float)
        self._max_step = self.config.get("max_step", int)

        self.ml_axis = None
        self.ml_encoder = None

        self._stop_event = Event()
        self._home_task = None
        self._move_task = None
        self._move_data = list()

    def initialize_hardware(self):
        try:
            self.pi_version = self.pi_get("*IDN?")
        except:
            self.pi_version = "UNKNOWN"

    def initialize_axis(self, axis):
        if self.ml_axis is not None and axis.name != self.ml_axis.name:
            raise RuntimeError("MLPZMotor only support one axis !!")
        self.ml_axis = axis
        self.ml_encoder = axis.encoder

    def initialize_hardware_axis(self, axis):
        self.pi_setservo(True)

    def __info__(self):
        info  = "controller : MLPZMotor\n"
        info += "- piezo   : {0}\n".format(self.pi_version)
        info += "- serial  : {0}\n".format(self.serial_url)
        info += "- encoder : {0} #{1}\n".format(self.ml_encoder.controller.name, self.ml_encoder.address)
        return info

    @property
    def max_nb_move(self):
        return self._max_nb_move

    @property
    def pos_deadband(self):
        return self._pos_deadband

    @property
    def max_step(self):
        return self._max_step

    @object_method(types_info=("None","None"))
    def show(self, axis):
        info  = """
ICEPAP encoder position   = {epos:.4f} um
PI piezo command position = {cpos:.4f} um
PI piezo sensor position  = {spos:.4f} um
"""
        epos = self.ml_encoder.read()
        cpos = self.pi_getcmdpos()
        spos = self.pi_getpos()
        print(info.format(epos=epos, cpos=cpos, spos=spos))

    def pi_get(self, cmd):
        byte_cmd = (cmd + "\n").encode()
        byte_ans = self.comm.write_readline(byte_cmd)
        ans = byte_ans.decode()
        return ans

    def pi_send(self, cmd):
        byte_cmd = (cmd + "\n").encode()
        byte_ans = self.comm.write(byte_cmd)
        err = self.pi_get("ERR?")
        return int(err)

    def pi_getpos(self):
        pos_str = self.pi_get("POS? A")
        return float(pos_str)

    def pi_getcmdpos(self):
        pos_str = self.pi_get("MOV? A")
        return float(pos_str)

    def pi_getservo(self):
        srv = self.pi_get("SVO? A")
        return int(srv)==1

    def pi_setservo(self, flag):
        val = flag is True and "1" or "0"
        err = self.pi_send("SVO A" + val)
        if err:
            msg = "Cannot set servo mode to {0}".format(flag)
            raise RuntimeError("MLPZMotor : {0} [error={1}]".format(msg, err))

    def pi_ontarget(self):
        ont = self.pi_get("ONT? A")
        return int(ont) == 1

    def pi_amove(self, pos, stop_ev=None):
        err = self.pi_send("MOV A {0:f}".format(pos))
        if err:
            msg = "Absolute move to {0:f} failed".format(pos)
            raise RuntimeError("MLPZMotor : {0} [error={1}]".format(msg, err))
        self.pi_wait(stop_ev)

    def pi_wait(self, stop_ev=None):
        while not self.pi_ontarget():
            if stop_ev is not None:
                if stop_ev.isSet():
                    return
            gevent.sleep(0.02)

    def pi_rmove(self, delta, stop_ev=None):
        err = self.pi_send("MVR A {0:f}".format(delta))
        if err:
            msg = "Relative move of {0:f} failed".format(pos)
            raise RuntimeError("MLPZMotor : {0} [error={1}]".format(msg, err))
        self.pi_wait(stop_ev)

    def reset(self, stop_ev):
        self.pi_amove(0.0, stop_ev)
        gevent.sleep(0.5)
        if stop_ev.isSet():
            return
        enc_pos = self.ml_encoder.read()
        self.ml_encoder.set(0.0)

    def moveloop(self, apos, stop_ev):
        # --- initial positions
        epos = self.ml_encoder.read()
        rpos = apos - epos
        self._move_data = list()
        self._move_data.append([apos, epos, rpos])

        if stop_ev.isSet():
            return

        # --- initial move cut using max_step
        if abs(rpos) > self._max_step:
            nstep = int(abs(rpos / self._max_step))
            step = self._max_step
            if rpos < 0.:
                step *= -1.
            for idx in range(nstep):
                self.pi_rmove(step, stop_ev)
            step = rpos - nstep * step
            self.pi_rmove(step, stop_ev)
        else:
            self.pi_rmove(rpos, stop_ev)

        # --- try to reach final position
        epos = self.ml_encoder.read()
        self._move_data.append([rpos, epos, apos-epos])
        ndone = 0

        while (abs(apos - epos) > self._pos_deadband) and (ndone < self._max_nb_move):
            if stop_ev.isSet():
                return
            rpos = apos - epos
            self.pi_rmove(rpos, stop_ev)
            gevent.sleep(0.02)
            epos = self.ml_encoder.read()
            self._move_data.append([rpos, epos, apos-epos])
            ndone += 1

        if (ndone == self._max_nb_move) and (abs(apos - epos) > self._pos_deadband):
            print("MLPZ WARNING > could not reach position after {0:d} moves.\n".format(ndone))

    @object_method(types_info=("None","None"))
    def report(self, axis):
        if not len(self._move_data):
            raise ValueError("No motion data in memory !!")

        print("Target position          = {0:.6f} um".format(self._move_data[0][0]))
        print("Initial encoder position = {0:.6f} um\n".format(self._move_data[0][1]))

        data = self._move_data[1:]
        head = ["PI MOVE", "ENCODER", "ERROR"]
        print(tabulate.tabulate(data, headers=head, floatfmt=".6f"))

    def read_position(self, axis):
        return self.ml_encoder.read()

    def state(self, axis):
        if self._move_task is not None:
            if bool(self._move_task):
                return AxisState("MOVING")
            elif not self._move_task.successful():
                raise self._move_task.exception

        if self.pi_ontarget():
            return AxisState("READY")
        return AxisState("MOVING")

    def home_search(self, axis, switch):
        self._stop_event.clear()
        self._home_task = gevent.spawn(self.reset, self._stop_event)

    def home_state(self, axis):
        if self._home_task is not None:
            if bool(self._home_task):
                return AxisState("MOVING")
            elif not self._home_task.successful():
                raise self._home_task.exception
        return AxisState("READY")

    def start_one(self, motion):
        if motion.axis != self.ml_axis:
            raise ValueError("MLPZ : invalid axis on move")
        self._stop_event.clear()
        self._move_task = gevent.spawn(self.moveloop, motion.target_pos, self._stop_event)

    def stop(self, axis):
        if self._home_task is not None:
            self._stop_event.set()
            self._home_task.join()
            self._home_task = None
            self._stop_event.clear()
        if self._move_task is not None:
            self._stop_event.set()
            self._move_task.join()
            self._move_task = None
            self._stop_event.clear()

    def set_velocity(self, axis, value):
        pass

    def read_velocity(self, axis):
        return 1.0

    def set_acceleration(self, axis, value):
        pass

    def read_acceleration(self, axis):
        return 1.0

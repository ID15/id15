# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy as np
from bliss.controllers.motor import CalcController
from bliss.common.hook import MotionHook
from bliss.config.settings import SimpleSetting

ECONST = 12.398425
ANGSTROMSIGN=u'\u212B'
DEGSIGN=u'\u00b0'
DELTASIGN=u'\u0394'
PLUSMINUSSIGN=u'\u00B1'

class LLEnergy(CalcController):
    def initialize(self):
        CalcController.initialize(self)
        config = self.config.config_dict
        self.mono = config["lauemono"]

    def initialize_axis(self, axis):
        self.mono.register_energy_axis(axis)

    def calc_from_real(self, positions_dict):
        l2rz_pos = positions_dict["l2rz"]
        energy_pos = self.mono.calc_energy(l2rz_pos)
        return dict(energy=energy_pos)

    def calc_to_real(self, positions_dict):
        energy = positions_dict["energy"]
        angle = self.mono.calc_angle(energy)
        distance = self.mono.calc_distance(energy)
        yoffset = self.mono.get_yoffset()
        return dict(l1rz=angle, l2rz=angle, l1x=distance, l2y=yoffset)

class TravelTimeHook(MotionHook):
    def __init__(self, name, config):
        self.name = name
        super(TravelTimeHook, self).__init__()

    def pre_move(self, motion_list):
        for motion in motion_list:
            axis = motion.axis
            estimated = abs(motion.delta) / axis.velocity
            if estimated > 1.0:
                print(f"WARNING: {axis.name} motion will last more than {estimated:.2f} sec")

    def post_move(self, motion_list):
        pass

class LaueLaueMono(object):
    FOILS_POS = dict(Gd=4, Hf=3, W=2, Au=1, Pb=0)
    FOILS_ENE = dict(Gd=50.239, Hf=65.351, W=69.525, Au=80.725, Pb=88.005)

    def __init__(self, name, config):
        self.name = name
        default_y_offset = config.get("yoffset", float)
        self.__y_offset = SimpleSetting(f"{name}:y_offset", default_value=default_y_offset)
        self.__d_spacing = config.get("dspacing", float)
       
        self.__foils_motor = config.get("foils_motor")
        self.__diode_motor = config.get("diode_motor")
#        self.__l1rz = 'l1rz'
        self.__energy_motor = None

    def register_energy_axis(self, axis):
        self.__energy_motor = axis
        
    def _energy_now(self):
        if self.__energy_motor is None:
            raise RuntimeError("No energy motor registered yet!")
        energy = self.__energy_motor.position
        angle = self.calc_angle(energy)
        return energy

    def energy_now(self):
        energy = self._energy_now()
        print(f"Energy is {energy:.3f} keV, Wavelength is {ECONST/energy:.4f} {ANGSTROMSIGN}")
        
    def calc_energy(self, angle):
        return ECONST / (2 * self.__d_spacing * np.sin(np.radians(angle)))
        
    def calc_distance(self, energy):
        angle = self.calc_angle(energy)
        return self.__y_offset.get() / np.tan(2 * np.radians(angle))
        
    def _ll_distance(self, energy=None, offset=None):
        if energy is None:
            energy = self.get_energy()
        if offset is None:
            offset = self.__y_offset.get()
        angle = self.calc_angle(energy)
        return offset / np.tan(2 * np.radians(angle))
        
    def calc_angle(self, energy):
        return np.degrees(np.arcsin(ECONST / 2.0 / energy / self.__d_spacing))

    def get_yoffset(self):
        return self.__y_offset.get()

    def set_yoffset(self, value):
        if self.__energy_motor is not None:
            energy = self.__energy_motor.position
        if value != self.__y_offset.get():
            self.__y_offset.set(value)
            if self.__energy_motor is not None:
                print("Updating laue-laue motors with new offset")
                self.__energy_motor.move(energy)
            else:
                print("No energy motor registered. Cannot update laue-laue motors.")
        else:
            print("No YOFFSET change !!")

    def list_foils(self):
        print("Foils available: (positions in lfiltrx)")
        print(f"{-1}   {'Empty'}")
        for i,(name, ene) in enumerate(self.FOILS_ENE.items()):
            print(f" {i}   {name:3.3s} ({ene:.3f} keV, {self.calc_angle(ene):.4f}{DEGSIGN})")

    def get_current_foil(self):
        foil_pos = self.__foils_motor.position
        foilname = None
        for (name, pos) in self.FOILS_POS.items():
            if abs(pos - foil_pos) < 0.05:
                foilname = name
        if(abs(-1-foil_pos) < 0.05):
        	foilname = 'Empty'
        if foilname is None:
            raise RuntimeError("Unknown foil motor position !!")
        return foilname

    def get_foil_energy(self):
        foilname = self.get_current_foil()
        return self.FOILS_ENE[foilname]

    def calc_diode_y(self, energy=None):
        # put the diode into the first crystal diffracted beam
        if energy is None:
            energy = self._energy_now()
#            print('Want to position the diode in l1rz beam')
#            energy=self.calc_energy(self.__l1rz.position)
#            if self.__energy_motor is None:
#                raise RuntimeError("No energy motor registered yet !!")
#            energy = self.__energy_motor.position
        distance = self.calc_distance(energy)
        angle = self.calc_angle(energy)
        diode_x = 310. + distance   # 310 is halfway between 
        diode_y = np.tan(2 * np.radians(angle)) * diode_x
        print(f'Diode (x,y) = ({diode_x:.2f},{diode_y:.2f})')
        return diode_y
        #self.__diode_motor.move(diode_y)

    def foil_in(self, foilname):
        if foilname not in self.FOILS_POS:
            raise ValueError("Unknown foil name. Should be one of {0}".format(tuple(self.FOILS_POS.keys())))
        foil_pos = self.FOILS_POS[foilname]
        self.__foils_motor.move(foil_pos)

    def diode_in(self, energy=None):
        diode_y = self.calc_diode_y(energy=None)
        print(f'Moving lfilty to {diode_y}')
        self.diode_motor.move(diode_y)      
        
    def foil_out(self):
        print(f'Moving {self.__foils_motor.name} to -1')
        self.__foils_motor.move(-1)
        
    def diode_out(self):
        print(f'Moving {self.diode_motor.name} to 69')        
        self.diode_motor.move(69)
    

        
        

    	




import numpy as np
from bliss.controllers.motor import CalcController

HC = 12.39847

class MLEnergy(CalcController):
    def initialize(self):
        CalcController.initialize(self)
        self.d = self.config.get("dspacing", float)
        self.L = self.config.get("mirror_distance", float)

    def calc_from_real(self, positions_dict):
        bragg = positions_dict["bragg"]
        theta = bragg/1000.
        if theta > 0.:
            energy = HC / (2. * self.d * np.sin(theta))
        else:
            energy = np.nan
        return dict(energy=energy)

    def calc_to_real(self, positions_dict):
        print(f'D spacing = {self.d}')
        print(f'Mirror distance = {self.L}')
        energy = positions_dict["energy"]
        theta = np.arcsin(HC / (2. * self.d * energy))
        # -- diffracted beam offset
        delta = self.L * np.tan(theta)
        # -- global offset
        offset = self.L * np.tan(2 * theta)
        # -- do not convert to deg mlrz is in mrad
        #bragg = np.degrees(theta)
        bragg = theta*1000 # mrad

        return dict(bragg=bragg, delta=delta, offset=offset)
       

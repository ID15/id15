from id15.utils import get_motor_position
import numpy as np
#
#
NMAX=50

yp2020= [-0.10282205,  3.26927105, -7.84062028]
yp2021= [-0.0913741,   3.0544204,  -6.86283359]
yp2021b=[-0.09104862,  3.08914654, -7.3905066 ]
yp2022= [-0.101,       3.230,      -7.611]

#
def calc_harmonics_quad(energy,yp=yp2022):
#    print(f'USing {[y for y in yp]:.4f}')
    print(f'Calculated for {energy:.1f} keV')
    print('Harmonic     Gap/mm      Power/kW')
    for n in range(1,NMAX):
        a=yp[0]
        b=yp[1]
        c=yp[2]-energy/n
        rt=b*b-4*a*c
        if rt>=0:
            rt=np.sqrt(rt)
        else:
            continue
#        print(n,(-b+rt)/2/a, ((-b-rt)/2/a))
        gap=(-b+rt)/2/a
        if gap >= 5:
            print(f'   {n:2d}        {round(gap,3):6.3f}       {round(undpower(1.8,energy/n),3):6.3f}')
        if gap >= 5.85 and n%2:
            best=(n,gap)
    return best


def undpower(l,e):

# l = device period [cm]
# e = Energy of fundamental
# k = 0.934 l b
# k = 1.8 (U18)
# p = 0.633 E_r^2 B_o^2 L I
# e_1 = 0.633 E_r^2 / l / (1+k^2/2)
# so that
# p = 0.633 E_r^2 L I k^2 / (0.934 l)^2
#   = 0.633 E_r^2 L I 2(0.950 E_r^2 / l - 1) / (0.934 l)^2
# Measure 3/33 of Gael LeBec 
#B = 3.207 * exp(1.077 * pi * gap / 18) + 0.9944 * exp(-3 * 1.1184 * pi * gap / 18)

    Er=6.0      # Ring Energy [Gev]
    L=1.5		# Device length [m]
    I=0.2		# Ring current [A]
    ksq = (0.95*Er*Er/l/e-1.)*2.0
    bsq = ksq / ( l*l*0.934*0.934)
    return 0.633*Er*Er*L*I*2*bsq


def undpeaks(energy=None):
    print('Parameters as of 11/22 Host lid15eh3')
    if energy == None:
        energy = get_motor_position('llen')
    best = calc_harmonics_quad(float(energy))
    print()
    print(f'Best harmonic seems to be {best[0]} at {best[1]:.3f}')
    return best[1]

#
#
#
#

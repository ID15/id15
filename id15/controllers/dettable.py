
import numpy

from bliss.common.hook import MotionHook
from bliss.common.logtools import log_debug
from bliss.physics.trajectory import LinearTrajectory

class DetTableCollision(Exception):
    pass

class DetTableHook(MotionHook):

    PosText = """
Positions:
    ddx {ddx:.2f} ; idx {idx:.2f} ; ddy {ddy:.2f} ; idy {idy:.2f}
"""

    SafeText = """
Safe conditions are:
       ddx > idx and (ddx - idx) > 1075.   [{status[0]}]
    or idx > ddx and (idx - ddx) > 700.    [{status[1]}]
    or idy > ddy and (idy - ddy) > 602.    [{status[2]}]
    or ddy > idy and (ddy - idy) > 430.    [{status[3]}]
"""
#    or idy > ddy and (idy - ddy) > 460.    [{status[2]}]
#    or ddy > idy and (ddy - idy) > 620.    [{status[3]}]

    def __init__(self, name, config):
        self.name = name
        super(DetTableHook, self).__init__()
        self.sampling_time = config.get("sampling_time", 0.025)
        self.__disable = False
        self.__inited = False

    def init(self):
        log_debug(self, "init")

        tags_needed = ["ddx", "ddy", "idx", "idy"]
        self.axes_role = dict()

        for axis in self.axes.values():
            axis_tags = axis.config.get("tags", default="")
            for tag in tags_needed:
                if axis.name == tag or tag in axis_tags:
                    self.axes_role[axis] = tag
                    log_debug(self, f"{tag} axis = {axis.name}")

        for tag in tags_needed:
            if tag not in self.axes_role.values():
                raise ValueError(f"DetTableHook: Missing {tag} axis in config")

        self.__inited = True

    def disable(self):
        """ Disable check for next move only """
        self.__disable = True

    def enable(self):
        self.__disable = False
 
    def pre_move(self, motion_list):
        log_debug(self, "pre_move")

        if self.__disable is True:
            log_debug(self, "disabled. No checks.")
            self.__disable = False
            return

        pos = dict([(tag, axis.dial) for axis,tag in self.axes_role.items()])
        traj = dict()
        for motion in motion_list:
            if motion.axis in self.axes_role:
                axis = motion.axis
                tag = self.axes_role[axis]
                target_pos = float(motion.target_pos) / float(axis.steps_per_unit)
                traj[tag] = LinearTrajectory(pos[tag],
                                             target_pos,
                                             axis.velocity,
                                             axis.acceleration,
                                             0.
                                            )

        maxtime = max([ t.duration for t in traj.values() ])
        for sample in numpy.arange(0., maxtime+self.sampling_time, self.sampling_time):
            tpos = dict(pos)
            for tag, t in traj.items():
                tpos[tag] = t.position(sample)
            self.__check_collision(tpos, sample)

    def __check_collision(self, tpos, sample=0.):
        safe = list()
        safe.append((tpos["ddx"] > tpos["idx"]) and ((tpos["ddx"] - tpos["idx"]) > 1075.))
        safe.append((tpos["idx"] > tpos["ddx"]) and ((tpos["idx"] - tpos["ddx"]) > 700.))
        #safe.append((tpos["idy"] > tpos["ddy"]) and ((tpos["idy"] - tpos["ddy"]) > 460.))
        #safe.append((tpos["ddy"] > tpos["idy"]) and ((tpos["ddy"] - tpos["idy"]) > 620.))
        safe.append((tpos["idy"] > tpos["ddy"]) and ((tpos["idy"] - tpos["ddy"]) > 602.))
        safe.append((tpos["ddy"] > tpos["idy"]) and ((tpos["ddy"] - tpos["idy"]) > 430.))
        log_debug(self, "position : %s", tpos)
        log_debug(self, "safe condition : %s", safe)

        if not any(safe):
            errmsg = "Possible Detector Table Collision at:\n"
            errmsg += self.PosText.format(**tpos)
            errmsg += self.SafeText.format(status=safe)
            raise DetTableCollision(errmsg)

        return safe

    def checkpos(self, ddx, ddy, idx, idy):
        tpos = dict(ddx=ddx, ddy=ddy, idx=idx, idy=idy)
        self.__check_collision(tpos)

    def __info__(self):
        if not self.__inited:
            self.init()

        msg = "Detector Table Collision Check Hook.\n"
        try:
            tpos = dict([(tag, axis.position) for axis,tag in self.axes_role.items()])
            msg += self.PosText.format(**tpos)
        except:
            msg += "\n!!! Cannot read motor positions !!!\n"
        try:
            safe = self.__check_collision(tpos)
        except:
            safe = ["Unknown",]*4

        msg += self.SafeText.format(status=safe)
        return msg

    def post_move(self, motion_list):
        log_debug(self, "post_move")
      

import tabulate
import gevent
from gevent.event import Event

from bliss.controllers.motor import Controller
from bliss.common.axis import AxisState
from bliss.comm.util import get_comm, TCP
from bliss.comm.tcp import SocketTimeout
from bliss.common.utils import object_method
from bliss import global_map


class NF8743Channel:
    def __init__(self, axis, ctrl):
        self.controller = ctrl
        self.name = axis.name
        self.channel = axis.config.get("channel", int)
        self.pos_deadband = axis.config.get("pos_deadband", int, 0)
        self.max_nb_move = axis.config.get("max_nb_move", int, 1)
        self.motor_type = 0
 
        self._move_data = list()

    def get(self, cmd):
        scmd = "{0:d}{1}".format(self.channel, cmd)
        return self.controller.get(scmd)

    def send(self, cmd):
        scmd = "{0:d}{1}".format(self.channel, cmd)
        self.controller.send(scmd)

    def state(self):
        state = AxisState()
        # -- check hardware limits
        hwlim = self.get("PH?")
        hwlim = int(hwlim)
        if self.channel == 1:
            if hwlim&0x01:
                state.set("LIMPOS")
            if hwlim&0x02:
                state.set("LIMNEG")
        elif self.channel == 2:
            if hwlim&0x08:
                state.set("LIMPOS")
            if hwlim&0x10:
                state.set("LIMNEG")
        # -- check move done
        mvdone = self.get("MD?")
        mvdone = int(mvdone)
        if mvdone == 0:
            state.set("MOVING")
        else:
            state.set("READY")
        return state

    def position(self):
        spos = self.get("TP?")
        return int(spos)

    def stop(self):
        self.send("ST")

    def reset(self):
        self.send("RS")

    def move(self, target_pos, stop_ev=None):
        self._move_data = list()
        ndone = 0
        tpos = int(target_pos)
        epos = self.position()
        rpos = tpos - epos
        self._move_data.append([epos, tpos])

        while abs(rpos) > self.pos_deadband and ndone < self.max_nb_move:
            self.send("PR{0}".format(rpos))
            gevent.sleep(0.02)
            while "MOVING" in self.state():
                if stop_ev is not None and stop_ev.isSet():
                    self.stop()
                    return
                gevent.sleep(0.02)
            epos = self.position()
            if abs(tpos - epos) <= self.pos_deadband:
                # --- let stabilize
                gevent.sleep(0.1)
                epos = self.position()
            self._move_data.append([rpos, epos])
            rpos = tpos - epos
            ndone += 1

    def report(self):
        if not len(self._move_data):
            raise ValueError("No motion data in memory !!")

        print("Initial position  = {0:d}".format(self._move_data[0][0]))
        print("Target position   = {0:d}\n".format(self._move_data[0][1]))

        data = self._move_data[1:]
        head = ["Relative move asked", "Real position"]
        print(tabulate.tabulate(data, headers=head))
            
class NF8743CL(Controller):
    DEFAULT_PORT = 23

    CTRL_ERRORS = {
      -1: "Unknown error code",
       0: "No errors",
       3: "Over temperature shutdown",
       6: "Command does not exist",
       7: "Parameter out of range",
       9: "Axis number out of range",
      10: "EEPROM write failed",
      11: "EEPROM read failed",
      37: "Axis number missing",
      38: "Command parameter missing",
      46: "RS-485 ETX fault detected",
      47: "RS-485 CRC fault detected",
      48: "Controller number out of range",
      49: "Scan in progress",
      51: "Homing started after reset",
    }
    AXIS_ERRORS = {
       0: "Motor type not defined",
       1: "Parameter out of range",
       3: "Following error threshold exceeded",
       4: "Positive Hardware limit detected",
       5: "Negative Hardware limit detected",
       6: "Positive Software limit detected",
       7: "Negative Software limit detected",
       8: "Motor not connected",
      10: "Maximum velocity exceeded",
      11: "Maximum acceleration exceeded",
      14: "Motion in progress",
      20: "Homing aborted",
      30: "Command not allowed during homing",
      33: "Max closed loop attempts exceeded",
      34: "Feedback connector removed",
      35: "Travel limit checking disabled",
      36: "Quasi-absolute encoder not connected",
      37: "Invalid cmd for quasi-absolute encoders",
    }
    MOTOR_TYPES = {
       0: "no motor connected",
       1: "unknown motor type",
       2: "tiny motor",
       3: "standard motor",
    }

    def initialize(self):
        config = self.config.config_dict
        self.comm = get_comm(config, TCP, port=self.DEFAULT_PORT, eol=b"\r\n", timeout=1.0)

        global_map.register(self, children_list=[self.comm])

        self._version = None
        self._connect_reply = b""
        self._channels = dict()

        self._move_task = None
        self._move_axis = None
        self._stop_event = Event()

    def initialize_hardware(self):
        self.comm.connect()
        reply = b""
        findchar= True
        while findchar:
            try:
                newchar = self.comm.read(1, timeout=0.05)
                reply += newchar
            except SocketTimeout:
                findchar = False
        self._connect_reply = reply

        self._version = self.get("*IDN?\r\n")

    def initialize_axis(self, axis):
        self._channels[axis] = NF8743Channel(axis, self)

    def initialize_hardware_axis(self, axis):
        chan = self._channels[axis]
        # --- is motor connected
        ans = chan.get("QM?")
        try:
            mottype = int(ans)
        except:
            raise RuntimeError("Cannot get motor type")
        if mottype == 0:
            raise RuntimeError("No motor connected")
        chan.motor_type = mottype
        # --- disable closed-loop
        chan.send("MM0")
        # --- drive motor in encoder counts
        chan.send("SN1")

    def get(self, cmd):
        reply = self.__ctrl_io(cmd + "\r\n")
        if not len(reply):
            serr = self.__ctrl_io("TE?\r\n")
            self.__error_check(serr)
        return reply

    def send(self, cmd):
        reply = self.__ctrl_io(cmd + ";TE?\r\n")
        self.__error_check(reply)

    def __ctrl_io(self, scmd):
        reply = self.comm.write_readline(scmd.encode())
        if reply.find(self._connect_reply) == 0:
            reply = reply[len(self._connect_reply):]
        return reply.decode()
 
    def __error_check(self, serr):
        if not len(serr):
            return
        try:
            ierr = int(serr)
        except:
            raise RuntimeError("Cannot decode error [{0}]".format(serr))
        if ierr == 0:
            # -- no error
            return
        if ierr < 100:
            # --- controller error
            errmsg = self.CTRL_ERRORS.get(ierr, "Unknown error")
            raise RuntimeError(errmsg)
        else:
            # --- axis error
            errmsg = "axis #{0:d} - {1}".format(
                         ierr // 100,
                         self.AXIS_ERRORS.get(ierr%100, "Unknown error"),
                     )
            raise RuntimeError(errmsg)

    def __info__(self):
        """
        return NEWFOCUS info
        """
        info_str = "NEWFOCUS INFO:\n"
        info_str += f"     host={self.comm._host}:{self.comm._port}\n"
        info_str += f"     version= {self._version}\n"
        return info_str

#     def get_axis_info(self, axis):
#         """
#         NEWFOCUS axis specific info
#         """
#         info_str = "NEWFOCUS AXIS:\n"
#         info_str += f"     channel={self.channel}\n"
#         info_str += f"     dead_band={self.pos_deadband}\n"
#         return info_str

    @property
    def version(self):
        if self._version is None:
            return "UNKNOWN"
        else:
            return self._version

    def state(self, axis):
        if self._move_task is not None and axis == self._move_axis:
            if bool(self._move_task):
                return AxisState("MOVING")
            elif not self._move_task.successful():
                raise self._move_task.exception
            else:
                self._move_task = None
        return self._channels[axis].state()

    def read_position(self, axis):
        return self._channels[axis].position()
       
    def stop(self, axis):
        if self._move_task is not None:
            self._stop_event.set()
            self._move_task.join()
            self._move_task = None
            self._move_axis = None
            self._stop_event.clear()
        else:
            self._channels[axis].stop()

    def start_one(self, motion):
        if self._move_task is not None:
            raise RuntimeError("move task already running")
        chan = self._channels[motion.axis]
        self._move_axis = motion.axis
        self._stop_event.clear()
        self._move_task = gevent.spawn(chan.move, motion.target_pos, self._stop_event)
 
    def set_velocity(self, axis, value):
        pass

    def read_velocity(self, axis):
        return 1.0

    def set_acceleration(self, axis, value):
        pass

    def read_acceleration(self, axis):
        return 1.0

    @object_method(types_info=("None", "str"))
    def motor_type(self, axis):
        mottype = self._channels[axis].motor_type
        return self.MOTOR_TYPES[mottype]

    @object_method(types_info=("None", "None"))
    def report(self, axis):
        self._channels[axis].report()

    @object_method(types_info=("None", "None"))
    def reset(self, axis):
        chan = self._channels[axis]
        chan.reset()
        gevent.sleep(1.)
        # --- disable closed-loop
        chan.send("MM0")
        # --- drive motor in encoder counts
        chan.send("SN1")


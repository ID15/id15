
from bliss.common.hook import MotionHook
from bliss.common.logtools import log_debug


class UndProtectHook(MotionHook):
    def __init__(self, name, config):
        self.name = name
        super(UndProtectHook, self).__init__()
        self._open_pos = dict()
        self._open_pos["U18"] = config.get("U18_open_position", 29.)
        self._open_pos["W76"] = config.get("W76_open_position", 139.)

    def init(self):
        log_debug(self, "init")

        tags = [ axis.name for axis in self.axes.values() ]
        errs = list()
        if "U18" not in tags:
            errs.append("Missing axis U18 for UndProtectHook")
        if "W76" not in tags:
            errs.append("Missing axis W76 for UndProtectHook")
        for tag in tags:
            if tag not in ["U18", "W76"]:
                errs.append(f"Wrong axis {tag} for UndProtectHook")
        if errs:
            raise RuntimeError(";".join(errs))

    def get_curr_pos(self):
        currpos = dict()
        for axis in self.axes.values():
            currpos[axis.name] = axis.position
        return currpos

    def pre_move(self, motion_list):
        movepos = self.get_curr_pos()
        movenames = list()
        for motion in motion_list:
            name = motion.axis.name
            movepos[name] = motion.target_pos / motion.axis.steps_per_unit
            movenames.append(name)

        errmsg = dict()
        errmsg["U18"] = "Undulator Protection: open W76 to close U18"
        errmsg["W76"] = "Undulator Protection: open U18 to close W76"
        if movepos["U18"] < self._open_pos["U18"] and movepos["W76"] < self._open_pos["W76"]:
            for name in movenames:
                if movepos[name] < self._open_pos[name]:
                    raise RuntimeError(errmsg[name])


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 10:14:42 2023

@author: blissadm
"""
import pyFAI
from bliss.common.plot import get_flint
from id15.utils import get_motor_position, get_motor
import json

P=0.172
Ny=1475
Nz=1679

def calc_yz(y,z):
    return y+P*Ny/2, z+P*Nz/2

def calc_poni():
    #ai=pyFAI.load(sys.argv[2])
    #x,y,z,energy,data=read_h5(sys.argv[1],'2.1')
    
    geometry={'poni1':0,'poni2':0, 'rot1':0, 'rot2':0, 'rot3':0, 'wavelength':0.1, 'dist':0}
    geometry['dist']=get_motor_position('ddx')/1000
    geometry['wavelength']=12.3984/get_motor_position('llen')/1e10

    y,z=calc_yz(get_motor_position('ddy'),get_motor_position('ddz'))
    geometry['poni1']=z/1000
    geometry['poni2']=(y-32)/1000
    
    return geometry

def read_poni(pfile):
    # this doesn't work since it can't find the detector definition file and throws an error
    #ai = pyFAI.load(pfile)
    with open(pfile,'r') as f:
        poni=f.readlines()
    geometry={'poni1':0,'poni2':0, 'rot1':0, 'rot2':0, 'rot3':0, 'wavelength':0.1, 'dist':0}
    for p in poni:
        v=p.split(':')
        if v[0] == 'Distance':
            geometry['dist']=float(v[1])
        elif v[0] == 'Poni1':
            geometry['poni1']=float(v[1])
        elif v[0] == 'Poni2':
            geometry['poni2']=float(v[1])
        elif v[0] == 'Rot1':
            geometry['rot1']=float(v[1])
        elif v[0] == 'Rot2':
            geometry['rot2']=float(v[1])
        elif v[0] == 'Rot3':
            geometry['rot3']=float(v[1])
        elif v[0] == 'Wavelength':
            geometry['wavelength']=float(v[1])
            
    return geometry


def setup_diff_plot_on_pilatus(poni=None):
    f = get_flint(creation_allowed=False, mandatory=False)
    if f is None:
        return
    p = f.get_live_plot(image_detector="pilatus")
    if poni is not None:
        geometry=read_poni(poni)
    else:
        geometry=calc_poni()
        
    print(json.dumps(geometry,indent=4))
    p.diffraction.set_detector("/users/blissadm/local/id15.git/id15/controllers/id15_pilatus.h5")
    #p.diffraction.set_geometry(dist=0.05, poni1=0.1, poni2=0.1, rot1=0.1745, rot2=0, rot3=0, wavelength=0.000000000103321)
    p.diffraction.set_geometry(**geometry)
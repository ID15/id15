# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 11:35:00 2020

@author: blissadm
"""
import numpy as np

from bliss.controllers.transfocator import Transfocator
from bliss.common.hook import MotionHook

Al2Be=1.586322766

class TransfocatorSlitCheck(Transfocator):
    def __init__(self, name, config):
        super().__init__(name, config)
        self.__slit_hgap = config.get("slit_hgap")
        self.__slit_vgap = config.get("slit_vgap")

    def check_allowed(self, value):
        if value > 0:
            hgap_pos = self.__slit_hgap.position
            vgap_pos = self.__slit_vgap.position
            if vgap_pos > 1. or hgap_pos > 1.:
                raise RuntimeError(f"Cannot insert {self.name} lenses if primary slits H/V gaps > 1.")

class SlitGapCheck(MotionHook):
    def __init__(self, name, config):
        super().__init__()
        self.name = name
        self.__tfname = config.get("transfocator")
        self.__tf = None

    def is_tfin(self):
        if self.__tf is None:
            from bliss.config.static import get_config
            config = get_config()
            self.__tf = config.get(self.__tfname)
        status = self.__tf.status_dict()
        return True in status.values()

    def pre_move(self, motion_list):
        check_tf = False
        pos = dict()
        for axis in self.axes.values():
            pos[axis.name] = axis.position
        for motion in motion_list:
            pos[motion.axis.name] = motion.target_pos
        for name, value in pos.items():
            if value > 1.:
                check_tf = True
        if check_tf and self.is_tfin():
                raise RuntimeError(f"Remove {self.__tfname} lenses before opening slits")

def tfguess(energy=None):
    vertical_pars=[-14.441,0.3588,0.007123,100.39,50.1848]
    horizontal_pars=[100.703,-4.387,0.0618,100.39,50.1848]
    if energy is None:
        from bliss.setup_globals import llen
        energy=llen.position
    print(f'For {energy:.3f} keV')
    print(f'Vertically:')
    _tfguess(energy,vertical_pars)
    print(f'Horizontally:')
    _tfguess(energy,horizontal_pars)
    
def _tfguess(energy,k):
    tot=k[0] + energy*k[1] + energy*energy*k[2]
    print(f'Total equivalent (Be100) lenses needed: {tot:.2f}')
    print('Try using axes ', end='')
    if tot>k[3] :
        print("10 ", end='')
        tot=tot-k[3]
    if tot>k[4] :
        print("9 ", end='')
        tot=tot-k[4]
    for n in np.linspace(5,0,6,dtype=np.int16):
        tlens=np.power(2,n)
        if tot>=tlens:
            print(f'{n+3:1d} ', end='')
            tot=tot-tlens
    if tot > 0.4:
        print('2 ',end='')
        tot=tot-0.5
    if tot > 0.1 :
        print('1',end='')
        tot=tot-0.2
    print()

def tfstatus():
    from bliss.setup_globals import tfoh1,tfoh2
    oh1=tfoh1.status_dict()
    oh2=tfoh2.status_dict()
    print()
    print('Axis 0 1 2 3 4 5 6 7 8 9 10')
    print('oh1  ',end='')
    [print('X ',end='') if oh1[a] else print('  ',end='') for a in oh1]
    print()
    print('oh2  ',end='')
    [print('X ',end='') if oh2[a] else print('  ',end='') for a in oh2]
    print()
    print()
    
def tfin(axis,*args):
    from bliss.setup_globals import tfoh1,tfoh2
    axis=str(axis)
    if len(args) == 1:
        try:
            lenses=list(args[0])
        except TypeError:
            lenses=list(args)
    else:
        lenses=args
    for lens in lenses:
        assert int(lens) <= 10, 'lens must be an integero 0-10'
        if axis in ['1','v','tf1','vert','tfoh1']:
            tfoh1.set_in(lens)
        elif axis in ['2','h','tf2','hor','tfoh2']:
            tfoh2.set_in(lens)
        else:
            print('Try something describing one of the transfocators')
    tfstatus()
    
def tfout(axis,*args):
    from bliss.setup_globals import tfoh1,tfoh2
    axis=str(axis)
    if len(args) == 1:
        try:
            lenses=list(args[0])
        except TypeError:
            lenses=list(args)
    else:
        lenses=args
    for lens in lenses:
        assert int(lens) <= 10, 'lens must be an integero 0-10'
        if axis in ['1','v','tf1','vert','tfoh1']:
            tfoh1.set_out(lens)
        elif axis in ['2','h','tf2','hor','tfoh2']:
            tfoh2.set_out(lens)
        else:
            print('Try something describing one of the transfocators')
    tfstatus()
    

    
def tfoutall(axis='0'):
    from bliss.setup_globals import tfoh1,tfoh2
    axis=str(axis)
    if axis == '0':
        tfoh1.set()
        tfoh2.set()
    elif axis in ['1','v','tf1','vert','tfoh1']:
        tfoh1.set()
    elif axis in ['2','h','tf2','hor','tfoh2']:
        tfoh2.set()
    else:
        print('Try something describing one of the transfocators')
    tfstatus()
    

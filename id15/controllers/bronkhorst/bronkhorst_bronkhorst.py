from gevent.lock import Semaphore
import sys
import gevent
import collections

from bliss import setup_globals
from bliss.comm.util import get_comm
from bm23.bronkhorst_lib import instrument, master, _PROPAR_MASTERS, PP_MODE_BINARY
from bm23.bronkhorst_lib import _propar_provider, _propar_builder, database

class _propar_provider_gevent(_propar_provider):
  """Implements the propar interface for master or slave"""

  def __init__(self, serial_comm, debug=False, dump=0, mode=PP_MODE_BINARY):
    """Implements the propar interface for the propar_slave/master class.
    Creates a serial connection that reads binary propar messages into a queue.
    The connection can also write messages to the serial connection.

    The read and write functions require a propar_message type, which is a
    dictionary with the following fields:
    propar_message['seq']       # Sequence Number (byte)
    propar_message['node']      # Node Address (byte)
    propar_message['len']       # Data Length (byte)
    propar_message['data']      # Data (list of bytes)

    dump 0 = no dump
    dump 1 = dump non-propar
    dump 2 = dump all
    """
    self.serial = serial_comm
 
    # External flags / options
    self.debug = debug
    self.dump  = dump
    self.mode  = mode

    # queues for propar data packets
    self.__propar_provider__receive_queue  = collections.deque()
    self.__propar_provider__transmit_queue = collections.deque()

    # receive variables
    self.__propar_provider__receive_buffer      = []
    self.__propar_provider__receive_state       = 0
    self.__propar_provider__receive_error_count = 0

    # propar binary receive states
    self.RECEIVE_START_1             = 0
    self.RECEIVE_START_2             = 1
    self.RECEIVE_MESSAGE_DATA        = 2
    self.RECEIVE_MESSAGE_DATA_OR_END = 3
    self.RECEIVE_ERROR               = 4

    # propar binary start/stop/control bytes
    self.BYTE_DLE = 0x10
    self.BYTE_STX = 0x02
    self.BYTE_ETX = 0x03

    # internal flags for reader thread
    self.run    = True
    self.paused = False

    # thread for reading serial data, will put all bytes into process_propar_byte
    self.serial_read_thread = gevent.spawn(self.serial_read_task)

  def serial_read_task(self):
    """This function is responsible for reading bytes from the serial port and
    processing the received bytes according to the binary propar protocol to
    build propar messages to put into the queue.
    This function must run in a thread.
    """
    while self.run:
      # try-except added to fix issues when we are stopped (comport is closed).
  	  # due to thread, this can cause read to error out.	  
      try:
        if self.paused == False:
          if self.serial.in_waiting:
            serial_data = self.serial.read(self.serial.in_waiting)
            for data_byte in serial_data:
              was_propar_byte = self.__propar_provider__process_propar_byte(data_byte)
              if self.dump != 0:
                if self.dump == 2 or was_propar_byte == False:
                  print(chr(data_byte), end='')
          else:
            gevent.sleep(0.001)
          if self.dump != 0:
            print(end='', flush=True)
        else:
          gevent.sleep(0.002)
      except:
        sys.excepthook(*sys.exc_info())
        gevent.sleep(0.002)

class master_gevent(master):
  def __init__(self, serial_comm):
    # serial propar interface, provides propar message dicts.
    self.propar = _propar_provider_gevent(serial_comm)

    # propar message builder
    self.propar_builder = _propar_builder()

    # database
    self.db = database()

    # debug flags
    self.debug_requests = False
    self.debug          = False

  	# callback for any propar broadcasts that are received
    self.broadcast_callback = lambda _: None

    # sequence number
    self.seq = 0
    # lock for sequence
    self.seq_lock = Semaphore()
    
    # list of active messages
    self._master__pending_requests   = []    
    self._master__processed_requests = []
    
    # 500 ms timeout on all messages
    self.response_timeout = 0.5
    
    # thread for processing propar messages
    self.msg_handler_thread = gevent.spawn(self._master__message_handler_task)

class instrument_gevent(instrument):
    def __init__(self, serial_comm, address=0x80):
        self.address = address
        self.serial_comm = serial_comm
        self.master = _PROPAR_MASTERS.setdefault(serial_comm, master_gevent(serial_comm))
        self.db = self.master.db
        

class Bronkhorst:
    
    def __init__(self, name, config):
    
        self._name = name
        self._config = config
        #self._serial_comm = get_comm(config, timeout=3, eol="\n\r")
        self._serial_comm = get_comm(config, timeout=3)
        #self._serial_comm.in_waiting = 1
        #self.instrument = instrument_gevent(self._serial_comm)


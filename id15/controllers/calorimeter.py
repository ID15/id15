
import xmlrpc.client
import gevent

class Calorimeter:
    def __init__(self, name, config):
        self._name = name
        self._address = config["address"]
        self._last_id = None
        self._last_exp = None
        self.connect()

    def connect(self):
        self._proxy = xmlrpc.client.ServerProxy(self._address)

        try:
            self._version = self._proxy.RegisterIP()
        except xmlrpc.client.Fault as err:
            raise RuntimeError(f"Calorimeter.RegisterIP FAULT message: {err.faultString}")

        try:
            self._module = self._proxy.GetActiveModule()
        except xmlrpc.client.Fault as err:
            raise RuntimeError(f"Calorimeter.GetActiveModule FAULT message: {err.faultString}")

    def __info__(self):
        txt = f"calorimeter address : {self._address}\n"
        #txt += f"software version    : {self._version}\n"
        txt += f"active module       : {self._module}\n"
        if self._last_id is not None:
            txt += f"Last order ID       : {self._last_id}\n"
        if self._last_exp is not None:
            txt += f"Last experiment :\n"
            for (name, value) in self._last_exp.items():
                txt += "    {name}= {value}\n"
        return txt

    @property
    def module(self):
        return self._module

    def putOrder(self, limsID, expStruct):
        for key in ["method", "sampleName", "chipSensorId"]:
            if key not in expStruct:
                raise ValueError(f"Missing {key} keyword")
        expStruct["moduleId"] = self._module
        try:
            self._proxy.PutOrder(limsID, expStruct)
            self._last_id = limsID
            self._last_exp = expStruct
        except xmlrpc.client.Fault as err:
            raise RuntimeError(f"Calorimeter.PutOrder FAULT message: {err.faultString}")
      
    def _check_id(self, limsID=None):
        if limsID is None:
            limsID = self._last_id
            if limsID is None:
                raise ValueError("Specify limsID. No one registedred yet.")
        return limsID
 
    def isDone(self, limsID=None):
        limsID = self._check_id(limsID)
        try:
            done = self._proxy.isDone(limsID)
            return done
        except xmlrpc.client.Fault as err:
            raise RuntimeError(f"Calorimeter.isDone FAULT message: {err.faultString}")

    def wait(self, limsID=None):
        limsID = self._check_id(limsID)
        while not self.isDone(limsID):
            print("waiting ...\r", end="")
            gevent.sleep(0.1)
        print("experiment done.")


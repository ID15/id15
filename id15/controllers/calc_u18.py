#!/users/vaughan/anaconda3/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import sys, os
import matplotlib.pyplot as plt

# x are the undulator gap
# n is the harmonic number

if len(sys.argv)>1:
    order=int(sys.argv[1])
else:
    order=2
    
NMAX=25
#[energy,n_harmonic,gap]
calc=np.array([[7.245,1,6],[14.489,2,6],[21.734,3,6],[28.978,4,6]])
e60=np.array([[60,6,7.02],[60,5,8.16],[60,4,10.32]])
e50=np.array([[50,6,6.1],[50,5,7.0],[50,4,8.50],[50,3,12.16]])
e507=np.array([[50.712,5,7.08],[50.712,4,8.6]])   
e77=np.array([[77,8,6.82],[77,7,7.56],[77,6,8.72],[77,5,10.68]])   
e825=np.array([[82.5,10,6.06],[82.5,9,6.54],[82.5,8,7.20],[82.5,7,8.03],[82.5,6,9.36],[82.5,5,11.92]]) 
e99=np.array([[99,10,6.96],[99,9,7.56],[99,8,8.42],[99,7,9.62]])
e101=np.array([[101,12,6.14],[101,11,6.56],[101,10,7.08],[101,9,7.70],[101,8,8.58],[101,7,9.84],[101,6,12.46]])

total=np.concatenate((e99,e825,e77,e60,e50,e507))

def calc_coeffs(order=2):
    x=total[:,2]
    y=np.divide(total[:,0],total[:,1])
    
    yp,res,_, _,_ =np.polyfit(x,y,order,full=True)
    p=np.poly1d(yp)
    xp=np.linspace(min(x)-1,max(x)+1,100)
    plt.plot(e101[:,2],np.divide(e101[:,0],e101[:,1]),'x',label='101 keV')
    plt.plot(e99[:,2],np.divide(e99[:,0],e99[:,1]),'x',label='99 keV')
    plt.plot(e825[:,2],np.divide(e825[:,0],e825[:,1]),'s',label='82.5 keV')
    plt.plot(e77[:,2],np.divide(e77[:,0],e77[:,1]),'o',label='77 keV')
    plt.plot(e60[:,2],np.divide(e60[:,0],e60[:,1]),'^',label='60 keV')
    plt.plot(e507[:,2],np.divide(e507[:,0],e507[:,1]),'s',label='50.712 keV')
    plt.plot(e50[:,2],np.divide(e50[:,0],e50[:,1]),'o',label='50 keV')
    plt.ion()
    plt.plot(xp,p(xp),'-',label='fit')
#    plt.plot(x,y-p(x), '-', label='error')
    plt.ylabel('E/n (keV)')
    plt.xlabel('Gap (mm)')
    plt.legend(loc='upper left')
    plt.text(min(x)+1,min(y),f'Polynomical Coefficicents: {[round(y,3) for y in yp]}')
    plt.text(min(x)+1,min(y)-1,f'Residual: {res}')
    plt.show()
    return(yp)

def calc_harmonics_linear(energy,yp):
    print(yp)
    for n in range(1,NMAX):
        g=energy/n-yp[1]
        g=g/yp[0]
        plt.plot(g,energy/n,'x')
        
def calc_harmonics_quad(energy,yp=[-0.10282205,  3.26927105, -7.84062028]):
#    print(yp)
    print()
    print('Harmonic     Gap/mm      Power/kW')
    for n in range(1,NMAX):
        a=yp[0]
        b=yp[1]
        c=yp[2]-energy/n
        rt=b*b-4*a*c
        if rt>=0:
            rt=np.sqrt(rt)
        else:
            continue
#        print(n,(-b+rt)/2/a, ((-b-rt)/2/a))
        gap=(-b+rt)/2/a
        if gap >= 5:
            print(f'   {n:2d}        {round(gap,3):6.3f}       {round(undpower(1.8,energy/n),3):6.3f}')
#        plt.plot(g,energy/n,'x')
        
        
def calc_harmonics(energy,yp):
    y=yp
    for n in range(1,NMAX):
        y[-1]=yp[-1]-energy/n
        print(n,np.roots(y))
        
        
def undpower(l,e):

# l = device period [cm]
# e = Energy of fundamental
# k = 0.934 l b
# k = 1.8 (U18)
# p = 0.633 E_r^2 B_o^2 L I
# e_1 = 0.633 E_r^2 / l / (1+k^2/2)
# so that
# p = 0.633 E_r^2 L I k^2 / (0.934 l)^2
#   = 0.633 E_r^2 L I 2(0.950 E_r^2 / l - 1) / (0.934 l)^2

    Er=6.0      # Ring Energy [Gev]
    L=1.5		# Device length [m]
    I=0.2		# Ring current [A]
    ksq = (0.95*Er*Er/l/e-1.)*2.0
    bsq = ksq / ( l*l*0.934*0.934)
    return 0.633*Er*Er*L*I*2*bsq


yp=calc_coeffs(order=order)
print(yp)

#print(yp[2]+yp[1]*8+yp[0]*8*8)

energy=60
while float(energy)>0:
    energy=input('Energy? ')
    if len(energy) == 0:
        break
    if order == 1:
        calc_harmonics_linear(float(energy),yp)
    elif order == 2:
        calc_harmonics_quad(float(energy),yp=yp)
        

        
        
        
        
        
        
# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy as np
from bliss.controllers.motor import CalcController

"""
LensTripod
==========

Lens aperture on top of a 3-axis positionner (id15eh2)
                
               ^
   (y)        leg
    ^        /(2)\
    |       /  -- \
    |      /   ||  \
    |     /    ||   \
    |----/-----||----\-------> (x)
    |   /      ||     \ 
    |  / (3)   ||   (1)\
    | /  leg   --   leg \
    |/________lens_______\
    |

Configurations:
* [leg1] [leg2] [leg3] are the three physical axis moving positive in z-direction
  and configured in mm.
* [dlegs] is the distance between legs expressed in mm (same distance between all legs)
* [dlens] is the height of lens aperture from tripod base

Calculated pseudos:
* [xtilt]/[ytilt] rotation around x/y axis in degrees
  fixed rotation point is at the middle of the tripod
* [zlens] is the heigth of the lens aperture in mm.
  [zlens] keeps constant when tilting the tripod around either x or y axis

Example:

controller:
    class: lenstripod
    module: tables
    dlegs: 50.0
    dlens: 35.51
    axes:
        - name: $t1
          tags: real leg1
        - name: $t2
          tags: real leg2
        - name: $t3
          tags: real leg3
        - name: tz
          tags: zlens
        - name: rx
          tags: xtilt
        - name: ry
          tags: ytilt
"""


class Lenstripod(CalcController):
    def initialize(self):
        CalcController.initialize(self)
        dlegs = self.config.get("dlegs", float)
        dlens = self.config.get("dlens", float)
        self.d1 = dlegs
        self.d2 = np.sqrt((dlegs ** 2) - ((dlegs / 2) ** 2))
        self.dz = dlens

    def calc_from_real(self, positions_dict):
        leg1 = positions_dict["leg1"]
        leg2 = positions_dict["leg2"]
        leg3 = positions_dict["leg3"]

        xtilt = np.arctan((leg2 - ((leg1 + leg3) / 2.)) / self.d2)
        ytilt = np.arctan((leg3 - leg1) / self.d1)
        zlegs = (leg1 + leg2 + leg3) / 3.
        self.zcorr = self.dz * (2 - np.cos(xtilt) - np.cos(ytilt))
        zlens = zlegs - self.zcorr

        return dict(xtilt=np.degrees(xtilt), ytilt=np.degrees(ytilt), zlens=zlens)

    def calc_to_real(self, positions_dict):
        xtilt = np.radians(positions_dict["xtilt"])
        ytilt = np.radians(positions_dict["ytilt"])
        zlens = positions_dict["zlens"]

        self.zcorr = self.dz * (2 - np.cos(xtilt) - np.cos(ytilt))
        zlegs = zlens + self.zcorr

        leg1 = zlegs - (np.tan(ytilt) * self.d1 / 2.) - (np.tan(xtilt) * self.d2 / 3.)
        leg2 = zlegs + (np.tan(xtilt) * self.d2 * 2 / 3.)
        leg3 = zlegs + (np.tan(ytilt) * self.d1 / 2.) - (np.tan(xtilt) * self.d2 / 3.)

        return dict(leg1=leg1, leg2=leg2, leg3=leg3)

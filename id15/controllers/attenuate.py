from id15.utils import get_motor_position, get_motor
import numpy as np
from scipy.interpolate import RectBivariateSpline

def attenuate(att, energy=None, move=False):
    if att >= 1:
        attpos=0
    else:
        if att < 3e-9 or att > 0.8:
            print('Maximum and Minimum attenuation (depending on energy) are 3e-9 and 0.8')
            print('Please enter a value in this range')
            return 0
        if energy is None: 
            energy = get_motor_position('llen')
        attpos=_attenuate(att, energy=energy)
        
        print(f'{att:.3g} attenuation for {energy:.3f} keV at atteh2 = {attpos:.2f}')
        if attpos < 6 or attpos > 200:
            print('6 < atteh2.position < 200')
            print('Desired attenuation is out of the possible range')
            print('Please try another value')
            return 0

    if move:
        attmotor = get_motor('atteh2')
        print(f'Moving atteh2 from {attmotor.position:.2f} to {attpos:.2f}')
#        attmotor=get_motor('atteh2')
        attmotor.move(attpos)
    return attpos

def _attenuate(att,energy=None):
    if energy is None:
        energy = get_motor_position('llen')
    e=np.linspace(40,100,7)
    xp0=np.array([  1.96256189,   3.90736349,   1.96885944,   0.3023884,    0.21076376,   0.02313895,  -0.04084972])
    xp1=np.array([-10.11722324, -14.66378051, -18.47639547, -21.49488826, -23.89158661, -25.78658389, -27.34057267])

    z=np.arange(-20,.1,.1)
    x=np.array([p0+p1*z for p0,p1 in zip(xp0,xp1)])
    interp_spline = RectBivariateSpline(e, z, x)
    return interp_spline(energy,np.log(att))[0][0]

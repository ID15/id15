from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset, ChainIterationPreset

class CountMuxPreset(ChainPreset):
    def __init__(self, opmux):
        self.opmux = opmux

    def prepare(self, chain):
        self.opmux.switch("PILATUS", "COUNTER_CARD")
        self.opmux.switch("EDGE", "COUNTER_CARD")
        self.opmux.switch("MAXIPIX", "COUNTER_CARD")
        self.opmux.switch("EDGEHS", "COUNTER_CARD")
        self.opmux.switch("MCA", "COUNTER_CARD")

class CountFastShutterPreset(ChainPreset):
    class Iterator(ChainIterationPreset):
        def __init__(self, parent):
            self.parent = parent

        def start(self):
            if self.parent.use_on_iter:
                self.parent.shutter.open()

        def stop(self):
            if self.parent.use_on_iter:
                self.parent.shutter.close()

    def __init__(self, shutter):
        self.shutter = shutter
        self.use_on_scan = False
        self.use_on_iter = False

    def get_iterator(self, chain):
        while True:
            yield CountFastShutterPreset.Iterator(self)

    def prepare(self, chain):
        self.use_on_scan = False
        self.use_on_iter = False
        if self.shutter.is_enabled_on_steps():
            self.use_on_iter = True
            print("FastShutter used at each steps")
        elif self.shutter.is_enabled():
            self.use_on_scan = True

    def start(self, chain):
        self.closeit = False
        if self.use_on_scan:
            if self.shutter.is_closed:
                self.closeit = True
                print("Opening FastShutter ...\033[K")
                self.shutter.open()

    def stop(self, chain):
        if self.closeit:
            print("Closing FastShutter ...\033[K")
            self.shutter.close()

class FScanMuxPreset(ScanPreset):
    def __init__(self, opmux):
        ScanPreset.__init__(self)
        self.opmux = opmux
        self.limadevs = list()

    def set_fscan_master(self, master):
        self.limadevs = master.lima_used + master.lima2_used
        self.mcadevs = master.mca_used

    def start(self, scan):
        for dev in self.limadevs:
            name = dev.name
            if name.startswith("pilatus"):
                self.opmux.switch("PILATUS", "MUSST")
                #dev._proxy.saving_statistics_log_enable = True
            elif name == "maxipix":
                self.opmux.switch("MAXIPIX", "MUSST")
            elif name == "pcoedgehs":
                self.opmux.switch("EDGEHS", "MUSST")
        if len(self.mcadevs):
            self.opmux.switch("MCA", "MUSST")

class FScanFastShutterPreset(ChainPreset):
    class Iterator(ChainIterationPreset):
        def __init__(self, parent):
            self.parent = parent

        def start(self):
            if self.parent.use_on_iter:
                print("Opening FastShutter ...\033[K", end="\r")
                self.parent.shutter.open()

        def stop(self):
            if self.parent.use_on_iter:
                print("Closing FastShutter ...\033[K", end="\r")
                self.parent.shutter.close()

    def __init__(self, shutter):
        self.shutter = shutter
        self.use_on_scan = False
        self.use_on_iter = False

    def set_fscan_master(self, master):
        self.use_on_iter = False
        try:
            fast_motor_mode = master.pars.fast_motor_mode
        except AttributeError:
            fast_motor_mode = None

        if fast_motor_mode == "REWIND" and \
           self.shutter.is_enabled() and self.shutter.is_closed:
               self.use_on_iter = True

    def get_iterator(self, chain):
        while True:
            yield FScanFastShutterPreset.Iterator(self)

    def prepare(self, chain):
        self.use_on_scan = False
        if not self.use_on_iter:
            if self.shutter.is_enabled() and self.shutter.is_closed:
                self.use_on_scan = True

    def start(self, chain):
        if self.use_on_scan and not self.use_on_iter:
            print("\nOpening FastShutter ...")
            self.shutter.open()

    def stop(self, chain):
        if self.use_on_scan and not self.use_on_iter:
            print("\nClosing FastShutter ...")
            self.shutter.close()


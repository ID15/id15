from bliss.scanning.chain import ChainPreset
from bliss.config.static import get_config
from bliss.common.logtools import log_warning

class LaueTempCheckPreset(ChainPreset):
    def __init__(self):
        self.wago = get_config().get("wcid15ah")
        self.key = "itlk_lltemp"

    def prepare(self, chain):
        value = self.wago.get("itlk_lltemp")
        if not value:
            log_warning(self, "Check LaueLaue temperature regulation")
            temp1, temp2 = self.wago.get("tl1_base", "tl2_base")
            log_warning(self, f"Base temperature are : {temp1:.2f} and {temp2:.2f}")


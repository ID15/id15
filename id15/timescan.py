import contextlib
from bliss.common.scans import timescan as orig_timescan
from bliss.common.scans import ct as orig_ct

@contextlib.contextmanager
def clean_chain(*args):
    from bliss.setup_globals import DEFAULT_CHAIN, chain_eh3_trig

    if len(args):
        chain_config = list(chain_eh3_trig["chain_config"])
        # remove unwanted items
        for name in args:
            arg_item = [ item for item in chain_config if item.get("device").name == name ][0]
            chain_config.remove(arg_item)
    else:
        chain_config = list()
     
    try:
        DEFAULT_CHAIN.set_settings(chain_config)
        yield
    finally:
        DEFAULT_CHAIN.set_settings(chain_eh3_trig["chain_config"])

@contextlib.contextmanager
def det_disable(detname):
    from bliss import current_session
    mg = current_session.active_mg
    detcnts = [ name for name in mg.enabled if name.startswith(f"{detname}:") ]

    try:
        for cnt in detcnts:
            mg.disable(cnt)
        yield
    finally:
        for cnt in detcnts:
            mg.enable(cnt)

def timescan(count_time: float, *args, **kwargs):
    with clean_chain():
        return orig_timescan(count_time, *args, **kwargs) 

def ct(count_time: float, *args, **kwargs):
    with clean_chain("pilatus4_lima1"):
        with det_disable("pilatus4"):
            return orig_ct(count_time, *args, **kwargs)


from bliss.config.settings import SimpleSetting
from bliss.scanning.chain import ChainPreset, ChainIterationPreset
from bliss.common.scans import DEFAULT_CHAIN

class ETMTCenterCorrectionPreset(ChainPreset):
    def __init__(self, etmtcorrobj):
        self._iter_preset = ETMTCenterCorrectionIterPreset(etmtcorrobj)

    def get_iterator(self, acq_chain):
        while True:
            yield self._iter_preset

class ETMTCenterCorrectionIterPreset(ChainIterationPreset):
    def __init__(self, etmtcorrobj):
        self.etmt_corr = etmtcorrobj

    def prepare(self):
        self.etmt_corr.correct_now()

class ETMTCenterCorrection:
    def __init__(self, name, config):
        self.__name = name

        self.__wago = config.get("wago")
        self.__wkey = config.get("wagokey")
        self.__motor = config.get("motor")
        
        self.__correction = SimpleSetting(f"{name}.correction", default_value=0.5)
        self.__zero_etmt = None
        self.__zero_motor = None

        self.__on_scan = False

    @property
    def name(self):
        return self.__name

    @property
    def correction_factor(self):
        return self.__correction.get()

    @correction_factor.setter
    def correction_factor(self, value):
        self.__correction.set(value)

    def _read_etmt(self):
        return self.__wago.get(self.__wkey)

    def _read_motor(self):
        return self.__motor.position

    def zero(self):
        self.__zero_etmt = self._read_etmt()
        self.__zero_motor = self._read_motor()

    def _check_zero(self):
        if self.__zero_etmt is None or self.__zero_motor is None:
            raise RuntimeError("Define zero positions before correction")
        if self.correction_factor == 0.:
            raise RuntimeError("Correction factor is null !!")

    def correct_now(self):
        self._check_zero()
        etmt_now = self._read_etmt()
        motor_now = self._read_motor()

        motor_pos = self.__zero_motor + self.correction_factor * (etmt_now - self.__zero_etmt)
        self.__motor.move(motor_pos)

    @property
    def correct_on_scan(self):
        return self.__on_scan

    @correct_on_scan.setter
    def correct_on_scan(self, flag):
        if flag is True:
            self._check_zero()
            DEFAULT_CHAIN.add_preset(ETMTCenterCorrectionPreset(self), "etmt_center")
            self.__on_scan = True
        else:
            DEFAULT_CHAIN.remove_preset(name="etmt_center")
            self.__on_scan = False

    def __info__(self):
        info = ""
        if self.__zero_etmt is None or self.__zero_motor is None:
            etmt_msg = "NOT DEFINED"
            mot_msg = "NOT DEFINED"
        else:
            etmt_msg = f"{self.__zero_etmt:.3f}"
            mot_msg = f"{self.__zero_motor:.3f}"

        info += f"\nzero etmt [{self.__wkey}] = {etmt_msg}"
        info += f"\nzero motor [{self.__motor.name}] = {mot_msg}"
        
        on_msg = self.__on_scan is True and "YES" or "NO"
        info += f"\n\nCorrection on scans = {on_msg}"
        return info

from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset

class CountMuxPreset(ChainPreset):
    def __init__(self, opmux):
        self.opmux = opmux

    def prepare(self, chain):
        self.opmux.switch("PERKIN", "COUNTER_CARD")
        self.opmux.switch("TRIGGER", "COUNTER_CARD")

class FScanMuxPreset(ScanPreset):
    def __init__(self, opmux):
        ScanPreset.__init__(self)
        self.opmux = opmux
        self.limadevs = list()

    def set_fscan_master(self, master):
        self.limadevs = master.lima_used
        self.mcadevs = master.mca_used
        self.musstname = master.musst.name

    def start(self, scan):
        self.opmux.switch("TRIGGER", "MUSST")
        if self.musstname == "musst_eh2_out":
            self.opmux.switch("MUSST", "MUSST_OUTSIDE")
        else:
            self.opmux.switch("MUSST", "MUSST_INSIDE")
        for dev in self.limadevs:
            name = dev.name
            if name == "perkin":
                self.opmux.switch("PERKIN", "MUSST")


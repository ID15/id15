# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""Laue-Laue bender monitor

Configuration example:

.. code-block: yaml

    class: MultiBenderMonitor
    module: llbender
    name: llbenders
    uri: id31pie712a:50000      # PI host:port
    benders:
      - channel: 1
        name: llpiezor1
        L: 119        # mm
        H: 28         # mm
        D: 82.66      # mm
        gamma: 13.32  # deg
        delta: 26     # deg
        C0: 0         # mm
      - channel: 2
        name: llpiezol1
        L: 119        # mm
        H: 28         # mm
        D: 82.66      # mm
        gamma: 13.32  # deg
        delta: 26     # deg
        C0: 0         # mm
      - channel: 4
        name: llpiezol2
        L: 119        # mm
        H: 28         # mm
        D: 82.66      # mm
        gamma: 13.32  # deg
        delta: 26     # deg
        C0: 0         # mm
      - channel: 5
        name: llpiezor2
        L: 119        # mm
        H: 28         # mm
        D: 82.66      # mm
        gamma: 13.32  # deg
        delta: 26     # deg
        C0: 0         # mm

"""

import time
import functools
import collections

try:
    OrderedDict = collections.OrderedDict
except AttributeError:
    from ordereddict import OrderedDict

import numpy
import gevent

from bliss.comm.tcp import Tcp
from bliss.common.event import send
from bliss.config.settings import SimpleSetting


def pi_cache(f):
    name = f.__name__
    functools.wraps(f)
    def wrapped(self):
        if name not in self.cache:
            return f(self)
        v = self.cache.get(name)
        if v is None:
            self.cache[name] = v = f(self)
        return v
    return wrapped


def get_comm(comm):
    if isinstance(comm, str):
        comm = Tcp(comm)
    return comm


class PI:

    def __init__(self, comm):
        self.comm = get_comm(comm)
        self.cache = { 'idn': None, 'tsc': None }

    @property
    @pi_cache
    def idn(self):
        return self.comm.write_readline(b'*IDN?\n')

    @property
    @pi_cache
    def tsc(self):
        return int(self.comm.write_readline(b'TSC?\n'))

    def query_multi_float(self, params=None, channels=None):
        channels = channels or list(range(1, self.tsc))
        nb_params, nb_channels = len(params), len(channels)
        nb_items = nb_params * nb_channels
        channels_str = ' '.join(map(str, channels))
        req = ''.join(['{0}? {1}\n'.format(param, channels_str)
                       for param in params])
        lines = self.comm.write_readlines(req.encode(), nb_items)
        answer = {}
        for i, param in enumerate(params):
            idx = i*nb_channels
            answer[param] = param_answer = {}
            for line in lines[idx:idx+nb_channels]:
                ch, value = line.decode().split('=', 1)
                ch, value = int(ch), float(value)
                param_answer[ch] = value
        return answer

    def get_tns(self, *channels):
        return self.query_multi_float(params=('TNS',), channels=channels)['TNS']

    def get_tsp(self, *channels):
        return self.query_multi_float(params=('TSP',), channels=channels)['TSP']

    def get_pos(self, *channels):
        return self.query_multi_float(params=('POS',), channels=channels)['POS']

    def __str__(self):
        return 'PI({0})'.format(self.comm) 

    def __repr__(self):
        return str(self)


def get_pi(param):
    if isinstance(param, PI):
        return param
    return PI(param)


class BenderMonitor:
    # L, H, D in mm; gamma and delta in degree

    def __init__(self, pi, channel=None, name='', L=119.0, H=28.0, D=82.66,
                 gamma=13.32, delta=26.0):
        # use micro-meter for distances and mili-radians for angles
        # except for *radius* where we use meter
        self.name = name
        self.pi = get_pi(pi)
        self.channel = channel
        self.L, self.H, self.D = L * 1E3, H * 1E3, D * 1E3
        self.gamma = numpy.radians(gamma)
        self.delta = numpy.radians(delta)
        self.mu = numpy.radians(90) - self.gamma - self.delta
        self.k = self.D * numpy.sin(self.mu) / self.H
        self._tsp_cache = None
        self.monitoring = None
        self.monitor_period = 0.1
        # C0 stored in micro-meter
        self._C0 = SimpleSetting('bender.' + name, default_value=0.0)

    def get_tsp(self, cache=True):
        if self.monitoring and self._tsp_cache:
            return self._tsp_cache
        return self.pi.get_tsp(self.channel)[self.channel]

    def get_c0(self, *args, **kwargs):
        """Returns C0 (micro-meter)"""
        return self._C0.get()

    def set_c0(self, c0):
        """Set C0 (micro-meter)"""
        self._C0.set(c0)
        #send(dict(c0=c0), 'value_changed', self)

    def get_c(self, tsp=None):
        """Returns C (micro-meter)"""
        tsp = self.get_tsp() if tsp is None else tsp
        return tsp + self.get_c0()

    def reset_c(self, tsp=None):
        tsp = self.get_tsp() if tsp is None else tsp
        self.set_c0(-tsp)

    def get_x(self, tsp=None):
        """Returns X (micro-meter). X = C.k"""
        return self.get_c(tsp=tsp) * self.k

    def get_alpha_radians(self, tsp=None):
        """Returns alpha (radians)"""
        return self.get_c(tsp=tsp) / self.H

    def get_alpha(self, tsp=None):
        """Returns alpha (mili-radians)"""
        return self.get_alpha_radians(tsp=tsp) * 1E3

    def get_r(self, tsp=None):
        """Returns bender curvature radius (meter)"""
        return self.L / (2 * self.get_alpha_radians(tsp=tsp)) * 1E-6

    def _monitor_loop(self):
        while True:    
            self._update_tsp(self.get_tsp(cache=False))
            gevent.sleep(self.monitor_period)

    def _update_tsp(self, tsp):
        self._tsp_cache = tsp
        event = dict(c=self.get_c(tsp=tsp),
                     x=self.get_x(tsp=tsp),
                     alpha=self.get_alpha(tsp=tsp),
                     r=self.get_r(tsp=tsp))
        send(event, 'value_changed', self)

    def _on_monitoring_end(self, task):
        self.monitoring = None
        self._tsp_cache = None

    def start_monitoring(self):
        if self.monitoring:
            raise RuntimeError('cannot start monitoring. It is already active') 
        task = gevent.spawn(self._monitor_loop)
        self.monitoring = task
        task.link(self._on_monitoring_end)

    def __int__(self):
        return self.channel

    def __str__(self):
        return 'BenderMonitor(channel={0.channel}, name={0.name!r})' \
               .format(self) 

    def __repr__(self):
        return 'BenderMonitor({0})'.format(self.name or self.channel)


def get_benders_map(pi, benders):
    benders = benders or {}
    pi = get_pi(pi)
    if not isinstance(benders, dict):
        _benders = OrderedDict()
        for bender in benders:
            if isinstance(bender, BenderMonitor):
                _benders[bender.channel] = bender
            elif isinstance(bender, dict):
                _benders[bender['channel']] = bender
            else:
                _benders[bender] = {}
        benders = _benders
    for channel in benders:
        bender = benders[channel]
        if not isinstance(bender, BenderMonitor):
            bender['channel'] = channel
            benders[channel] = BenderMonitor(pi, **bender)
    return benders


class MultiBenderMonitor:

    def __init__(self, pi, name='', benders=None, channels=None):
        pi = get_pi(pi)
        self.pi = pi
        self.name = name
        self.benders = get_benders_map(pi, benders)
        self.benders.update(get_benders_map(pi, channels))
        self.channels = list(self.benders.keys())
        self._tsp_cache = None
        self.monitoring = None
        self.monitor_period = 0.1

    def __getattr__(self, name):
        # defer low level questions to PI
        return getattr(self.pi, name)

    def _get_tsp_param(self, param, channels=None, tsp=None):
        param = param.lower()
        channels = channels or self.channels
        benders = self.benders
        tsp = self.get_tsp(channels=channels) if tsp is None else tsp
        answer = {}
        for channel in channels:
            f = getattr(benders[channel], "get_" + param)
            bender = benders[channel]
            answer[bender] = f(tsp=tsp[bender])
        return answer

    def get_tsp(self, channels=None, cache=True):
        if cache and self._tsp_cache:
            tsp = self._tsp_cache
            if channels is not None:
                result = OrderedDict()
                for channel in channels:
                    bender = self.benders[channel]
                    result[bender] = tsp[bender]
                return result
        else:
            channels = channels or self.channels
            tsp = self.pi.get_tsp(*channels)
            tsp = OrderedDict([(self.benders[channel], tsp[channel])
                               for channel in channels])
        return tsp
    
    def get_c(self, **kwargs):
        return self._get_tsp_param('c', **kwargs)

    def get_c0(self, **kwargs):
        return self._get_tsp_param('c0', **kwargs)
        #channels = channels or self.channels
        #answer = dict()
        #for channel in channels:
        #    answer[bender] = self.benders[channel].get_c0()
        #return answer

    def set_c0(self, **kwargs):
        raise NotImplementedError

    def reset_c(self, channels=None):
        """Sets the C to zero (ie, changes C0 so that C becomes 0)"""
        channels = channels or self.channels
        tsp = get_tsp(channels=channels, cache=False)
        for bender, bender_tsp in list(tsp.items()):
            bender.reset_c(tsp=bender_tsp)

    def get_x(self, **kwargs):
        return self._get_tsp_param('x', **kwargs)

    def get_alpha_radians(self, **kwargs):
        return self._get_tsp_param('alpha_radians', **kwargs)

    def get_alpha(self, **kwargs):
        return self._get_tsp_param('alpha', **kwargs)

    def get_r(self, **kwargs):
        return self._get_tsp_param('r', **kwargs)

    def _monitor_loop(self):
        while True:
            start = time.time()
            self.trigger_single_read()
            elapsed = time.time() - start
            nap_time = max(1E-3, self.monitor_period - elapsed)
            gevent.sleep(nap_time)

    def trigger_single_read(self):
        self._update_tsp(self.get_tsp(cache=False))

    def _update_tsp(self, tsp):
        self._tsp_cache = tsp
        for bender, tsp_item in list(self._tsp_cache.items()):
            bender._update_tsp(tsp_item)
        event = dict(c=self.get_c(tsp=tsp),
                     x=self.get_x(tsp=tsp),
                     alpha=self.get_alpha(tsp=tsp),
                     r=self.get_r(tsp=tsp))
        send(self, 'value_changed', event)

    def _on_monitoring_end(self, task):
        self.monitoring = None
        self._tsp_cache = None
        for bender in list(self.benders.values()):
            bender._on_monitoring_end(task)

    def start_monitoring(self):
        if self.monitoring:
            raise RuntimeError('cannot start monitoring. It is already active') 
        benders = set(self.benders.values())
        for bender in benders:
            if bender.monitoring:
                bid = bender.name or bender.channel
                raise RuntimeError('cannot start monitoring. Bender %r ' \
                                   'is already a monitoring' % bid )
        task = gevent.spawn(self._monitor_loop)
        self.monitoring = task
        for bender in benders:
            bender.monitoring = task
        task.link(self._on_monitoring_end)
        return task

    def stop_monitoring(self):
        if self.monitoring:
           self.monitoring.kill()


def create_bender_from_config(config):
    klass = config['class']
    uri = config['uri']
    name = config.get('name', '')
    if klass == 'MultiBenderMonitor':
        benders = [dict(bender) for bender in config.get('benders', {})]
        return MultiBenderMonitor(uri, name=name, benders=benders)
    elif klass == 'BenderMonitor':
        return BenderMonitor(uri, name=name)
    else:
        raise TypeError('unknown bender class %r' % klass)


def create_bender_from_name(name): 
    from bliss.config.static import get_config
    config = get_config()
    return create_bender_from_config(config.get_config(name))


def main():
    import sys
    from pprint import pprint as pp
    from bliss.common.event import connect

    mb = create_bender_from_name(sys.argv[1])
    
    print('--- get several values ---')
    print('C=', pp(mb.get_c()))
    print('X=', pp(mb.get_x()))
    print('alpha=', pp(mb.get_alpha()))
    print('R=', pp(mb.get_r()))

    print('--- get consistent values ---')
    tsp = mb.get_tsp()
    print('C=', pp(mb.get_c(tsp=tsp)))
    print('X=', pp(mb.get_x(tsp=tsp)))
    print('alpha=', pp(mb.get_alpha(tsp=tsp)))
    print('R=', pp(mb.get_r(tsp=tsp)))
    
    print('--- monitor for 1 second (10x per second) ---')
    def print_event(event):
        print('--- EVENT ---')
        pp(event)
    connect(mb, 'value_changed', print_event)
    mb.start_monitoring()
    gevent.sleep(1)

if __name__ == '__main__':
    main()

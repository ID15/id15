from PyTango import DeviceProxy, DevState
from bliss.scanning.chain import ChainPreset
import tabulate

class LimaRateProtection:
    def __init__(self, name, config):
        self.name = name
        self.__lima = config.get("device")
        self.__proxy = None
        self.__preset = None

    @property
    def proxy(self):
        if self.__proxy is None:
            devname = self.__lima.proxy.getPluginDeviceNameFromType("rateprotection")
            if not devname:
                raise RuntimeError("Cannot get rate protection device from {self.__lima.name}")
            self.__proxy = DeviceProxy(devname)
        return self.__proxy
       
    @property
    def preset(self):
        if self.__preset is None:
            self.__preset = LimaRateProtectionPreset(self.__lima, self)
        return self.__preset
        
    def is_active(self):
        state = self.proxy.State()
        return state == DevState.ON

    def start(self):
        self.proxy.Start()

    @property
    def hotpixels(self):
        data = self.__proxy.getTriggeredPixels()
        if len(data):
            data.shape = (len(data)//2, 2)
        return data

    def __info__(self):
        is_on = self.is_active()
        status = is_on is True and "ON" or "OFF"
        info = f"[{self.__lima.name}] rate protection is {status}\n"
        if is_on:
            trigrate = self.proxy.maximum_count_rate
            info += f" * Protection max count rate = {trigrate}\n"
            trigged = self.proxy.is_triggered
            if trigged:
                framenr = self.proxy.frame_number
                info += f" * Protection TRIGGERED on frame #{framenr}\n"
            else:
                info += f" * Protection NOT triggered\n"

            warnrate = self.proxy.warning_count_rate
            if warnrate:
                info += f" * Warning max count rate = {warnrate}\n"
                warned = self.proxy.is_warning_triggered
                if warned:
                    framenr = self.proxy.warning_frame_number
                    info += f" * Warning TRIGGERED on frame #{framenr}\n"
                else:
                    info += f" * NO warning triggered\n"

            if trigged:
                data = self.hotpixels
                info += "\nHot pixels:\n"
                info += tabulate.tabulate(data, headers=("X", "Y"))
                info += "\n"
        return info

class LimaRateProtectionPreset(ChainPreset):
    def __init__(self, limaobj, rateobj):
        self.__lima = limaobj
        self.__rate = rateobj
        self.__started = False

    def prepare(self, chain):
        if list(chain.get_node_from_devices(self.__lima))[0] is not None:
            self.__rate.proxy.Start()
            self.__started = True
        else:
            self.__started = False

    def stop(self, chain):
        if self.__started:
            name = self.__lima.name
            rdev = self.__rate.proxy
            if rdev.is_triggered:
                framenr = rdev.frame_number
                raise RuntimeError(f"{name} protection has triggered at frame #{framenr}")
            elif rdev.is_warning_triggered:
                framenr = rdev.warning_frame_number
                wrate = rdev.warning_count_rate
                print(f"\n!!! {name} protection WARNING !!!")
                print(f"\tcount rate exceeds {wrate} at frame #{framenr}")

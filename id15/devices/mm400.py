import functools
import gevent

class MM400:
    COMMANDS = {
        "start": "mm_start",
        "stop": "mm_stop",
        "time_up": "mm_time_u",
        "time_down": "mm_time_d",
        "freq_up": "mm_freq_u",
        "freq_down": "mm_freq_d",
        "set": "mm_set",
        "prog": "mm_prog",
    }

    def __init__(self, name, cfg_node):
        self.name = name
        self._wago = cfg_node.get("wago")
        for cmdname in self.COMMANDS.keys():
            setattr(self, cmdname, functools.partial(self.__command, cmdname))

    def __command(self, cmd):
        try:
            wkey = self.COMMANDS[cmd]
        except:
            raise ValueError("MM400 Invalid command [{0}]".format(cmd))
        self._wago.set(wkey, 1)
        gevent.sleep(0.2)
        self._wago.set(wkey, 0)

    def reset(self):
        for wkey in self.COMMANDS.values():
            self._wago.set(wkey, 0)

    def list(self):
        return list(self.COMMANDS.keys())

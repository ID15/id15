from bliss.config.settings import SimpleSetting
from bliss.common.shutter import BaseShutter, BaseShutterState

class FastShutter(BaseShutter):
    def __init__(self, name, config):
        self.__name = name
        self.__comfig = config
        self.__motor = config["motor"]
        self.__enabled = SimpleSetting(f"{self.__name}_enabled", default_value=True)
        self.__step_mode = SimpleSetting(f"{self.__name}_step_mode", default_value=False)

    @property
    def name(self):
        return self.__name

    @property
    def config(self):
        return self.__config

    def __info__(self):
        scan_state = self.is_enabled() and "YES" or "NO"
        step_mode = self.is_enabled_on_steps() and "YES" or "NO"
        info = f"Shutter state    = {self.state_string}\n"
        info += f"Motor position   = {self.__motor.position} [{self.__motor.name}]\n"
        info += f"Enabled on scans = {scan_state}\n"
        info += f"Enabled on steps = {step_mode}\n"
        return info

    def open(self, silent=False):
        self.__motor.move(1.0)

    def close(self, silent=False):
        self.__motor.move(0.0)

    def enable(self):
        self.__enabled.set(True)

    def disable(self):
        self.__enabled.set(False)
        self.__step_mode.set(False)

    def is_enabled(self):
        return self.__enabled.get()

    def enable_on_steps(self):
        self.enable()
        self.__step_mode.set(True)

    def disable_on_steps(self):
        self.__step_mode.set(False)

    def is_enabled_on_steps(self):
        return self.__step_mode.get()

    @property
    def state(self):
        if self.__is_opened():
            return BaseShutterState.OPEN
        else:
            return BaseShutterState.CLOSED

    def __is_opened(self):
        return abs(self.__motor.position - 1.0) < 0.001



from bliss.config.settings import SimpleSetting
from bliss.scanning.scan import ScanPreset
from bliss.common.logtools import log_warning


class PicoFixedRange:
    def __init__(self, name, config):
        self.__pico = config.get("pico")
        self.__value = SimpleSetting(f"{name}_value", default_value=0.001)
        self.__enabled = SimpleSetting(f"{name}_enabled", default_value=True)
        self.__preset = None

    def __info__(self):
        info = f"pico name        : {self.pico_name}\n"
        info += f"fixed range      : {self.get()}\n"
        info += f"Enabled on fscan : {self.is_enabled()}\n"
        return info

    @property
    def pico_name(self):
        return self.__pico.name

    def enable(self):
        self.__enabled.set(True)

    def disable(self):
        self.__enabled.set(False)

    def is_enabled(self):
        return self.__enabled.get()

    def set(self, value):
        self.__value.set(value)

    def get(self):
        return self.__value.get()

    @property
    def preset(self):
        if self.__preset is None:
            self.__preset = PicoFixedRangePreset(self)
        return self.__preset

    def set_fixed_range(self):
        self.__pico.auto_range = False
        self.__pico.range = self.get()

    def unset_fixed_range(self):
        self.__pico.auto_range = True


class PicoFixedRangePreset(ScanPreset):
    def __init__(self, picofixedrange):
        self._dev = picofixedrange

    def set_fscan_master(self, master):
        pass

    def prepare(self, scan):
        if self._dev.is_enabled():
            try:
                self._dev.set_fixed_range()
            except Exception:
                log_warning(self, f"Failed to set fixed range on {self._dev.pico_name}")


    def stop(self, scan):
        if self._dev.is_enabled():
            try:
                self._dev.unset_fixed_range()
            except Exception:
                log_warning(self, f"Failed to set back autorange on {self._dev.pico_name}")


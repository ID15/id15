import tabulate

from bliss.common.utils import grouped
from bliss.controllers.counter import SamplingCounterController
from bliss.common.counter import SamplingCounter
from bliss.common.protocols import CounterContainer

class WagoValvesCounterController(SamplingCounterController):
    def __init__(self, device):
        super().__init__(f"{device.name}")
        self.device = device

        for idx in range(self.device.nvalves):
            name = f"valve{idx}"
            cnt = self.create_counter(SamplingCounter, name, mode="SINGLE")
            cnt.valve = idx

    def read_all(self, *cnts):
        values = self.device.read_state()
        return [ values[cnt.valve] for cnt in cnts ]
        

class WagoValves(CounterContainer):

    def __init__(self, name, cfg_node):
        self.name = name
        self._wago = cfg_node.get("wago")
        self._in_key = cfg_node.get("valve_in")
        self._out_key = cfg_node.get("valve_out")
        self._nvalves = cfg_node.get("nvalves", int)

        self._cc = WagoValvesCounterController(self)

    def __info__(self):
        data = []
        for idx in range(self._nvalves):
            data.append([idx, self.state(idx)])
        head = ["Valve Number", "State"]
        return tabulate.tabulate(data, headers=head)

    def show(self):
        print(self.__info__())

    def setA(self, valve_number):
        values = self._wago.get(self._out_key)
        values[2*valve_number:2*valve_number+2] = [0, 1]
        self._wago.set(self._out_key, values)

    def setB(self, valve_number):
        values = self._wago.get(self._out_key)
        values[2*valve_number:2*valve_number+2] = [1, 0]
        self._wago.set(self._out_key, values)

    def state(self, valve_number):
        value = self.read_state()[valve_number]
        val2str = {0: "A", 1: "B", -1: "UNKNOWN"}
        return val2str[value]

    def old_state(self, valve_number):
        values = self._wago.get(self._in_key)
        valve_value = values[2*valve_number:2*valve_number+2]
        if valve_value == [1, 0]:
            return "B"
        elif valve_value == [0, 1]:
            return "A"
        else:
            return "UNKNOWN"

    def read_state(self):
        values = self._wago.get(self._in_key)
        res = list()
        for val in grouped(values, 2):
            if val == (0, 1):
                res.append(0)
            elif val == (1, 0):
                res.append(1)
            else:
                res.append(-1)
        return res[0:self._nvalves]
                
    @property
    def nvalves(self):
        return self._nvalves

    @property
    def counters(self):
        return self._cc.counters

import tabulate
from fscan.fscantools import FScanParamBase


class XRDCTClass(object):
    def __init__(self):
        self.__detectors = ("pilatus", )
        default_pars = dict(
            readout_time = 0.00095,
            safety_time = 0.005,
            omega_start = 0,
            rot_center = 0.,
        )
        self.pars = FScanParamBase("xrdct", default_pars)
        
    def __call__(self, sample_size, scan_range, y_resolution, expo_time, z_start=None, z_end=None, z_step=None):
        kwargs = dict(z_start=z_start, z_end=z_end, z_step=z_step)
        self.run(sample_size, scan_range, y_resolution, expo_time, **kwargs)

    def __info__(self):
        info = """\n
Usage: xrdct(sample_size, scan_range, y_resolution, expo_time)
       xrdct(sample_size, scan_range, y_resolution, expo_time, z_start, z_end, z_step)
"""
        return info

    def run(self, sample_size, scan_range, y_resolution, expo_time, **kwargs):
        # --- check z-axis parameters
        z_start = kwargs.pop("z_start", None)
        z_end = kwargs.pop("z_end", None)
        z_step = kwargs.pop("z_step", None)

        z_pars = (z_start is not None, z_end is not None, z_step is not None)
        if all(z_pars):
            use_z = True
        elif any(z_pars):
            raise ValueError("If using z-axis, needs to give all parameters : z_start, z_end, z_step")
        else:
            use_z = False

        # --- set optionnal parameters
        if len(kwargs):
            self.pars.from_dict(kwargs)

        # --- check detector
        dets = [ name.split(":")[0] for name in ACTIVE_MG.enabled if name.endswith(":image") ]
        if not len(dets):
            raise RuntimeError("xrdct: No active camera !! Check ACTIVE_MG.")
        if len(dets) > 1:
            raise RuntimeError("xrdct: More than one camera active !! Check ACTIVE_MG.")
        if dets[0] not in self.__detectors:
            raise RuntimeError(f"xrdct: Active camera should be one of {self.__detectors}")
        
        # --- calculate parameters
        pars = self.pars

        n_omega = int(sample_size / y_resolution) + 1
        if n_omega == 1:
            omega_step = 0.
        else:
            omega_step = 180 / (n_omega - 1)

        y_step_size = y_resolution
        y_step_time = expo_time + pars.readout_time + pars.safety_time
        y_npoints = int(scan_range / y_resolution) + 1
        acq_size = y_resolution * (expo_time / y_step_time)
        y_start = pars.rot_center - (scan_range / 2.) - (acq_size / 2)

        # --- prepare fscan parameters
        # z-axis
        if use_z:
            fscan3d.pars.slow1_motor = hrz
            fscan3d.pars.slow1_start_pos = z_start
            if z_start > z_end:
                fscan3d.pars.slow1_step_size = -1. * abs(z_step)
            else:
                fscan3d.pars.slow1_step_size = abs(z_step)
            fscan3d.pars.slow1_npoints = int(abs((z_end-z_start)/z_step))
        else:
            fscan3d.pars.slow1_motor = hrz
            fscan3d.pars.slow1_start_pos = hrz.position
            fscan3d.pars.slow1_step_size = 0.
            fscan3d.pars.slow1_npoints = 1

        # rotation 
        fscan3d.pars.slow2_motor = hrrz
        fscan3d.pars.slow2_start_pos = pars.omega_start
        fscan3d.pars.slow2_step_size = omega_step
        fscan3d.pars.slow2_npoints = n_omega

        # y-axis
        fscan3d.pars.fast_motor = hry
        fscan3d.pars.fast_start_pos = y_start
        fscan3d.pars.fast_step_size = y_step_size
        fscan3d.pars.fast_step_time = y_step_time
        fscan3d.pars.fast_npoints = y_npoints
        fscan3d.pars.acq_time = expo_time
      
        fscan3d.pars.display_flag = False

        # --- tomo sequence
        print("absct >> Opening EH3 shutter ...")
        try:
            sheh3.open()
        except RuntimeError:
            print("!! Failed to OPEN EH3 SHUTTER !!")
            print(sheh3.__info__())

        print("absct >> TOMO scan ...")
        fsh.move(1.)
        fscan3d.run()
        fsh.move(0.)

        print("absct >> Closing EH3 shutter ...")
        try:
            sheh3.close()
        except RuntimeError:
            pass

xrdct = XRDCTClass()



def ll_edge_scan(foilname):
    print(f"set foil {foilname} ...")
    lauemono.set_foil(foilname)
    foil_e = lauemono.get_foil_energy(foilname)

    print(f"set laue energy to {foil_e:.3f} keV ...")
    llenergy.move(foil_e)

    print(f"move 2nd crystal off")
    umvr(l2rz, -0.1)

    print(f"move diode in")
    lauemono.set_diode_in(foil_e)

    print(f"Launch scan")
    diode_x = 310. + l1x.position
    diode0 = np.tan(2 * np.radians(angle-0.01) * diode_x
    diode1 = np.tan(2 * np.radians(angle+0.01) * diode_x
    diode_half_range = abs(diode1 - diode0) / 2.

    d2scan(l1rz, -.01, .01, lfilty, -1.*diode_half_range, diode_half_range, 50, .2)

import tabulate
from fscan.fscantools import FScanParamBase

class ABSCTClass(object):
    def __init__(self):
        self.__detectors = ("pcoedge", "pcodimax", "pcoedgehs")
        default_pars = dict(
            readout_time = 0.015,
            omega_start = 0,
            nflat = 100,
            ndark = 100,
            n_over_scan = 100,
            rot_center = 0.,
            flat_pos = -10.,
            rot_axis_at_sensor_edge = False,
        )
        self.pars = FScanParamBase("absct", default_pars)
        
    def __call__(self, expo_time, nproj, rot_center=None, flat_pos=None, rot_axis_at_sensor_edge=None):
        kwargs = dict()
        if rot_center is not None:
            self.pars.rot_center = rot_center
        if flat_pos is not None:
            self.pars.flat_pos = flat_pos
        if rot_axis_at_sensor_edge is not None:
            self.pars.rot_axis_at_sensor_edge = rot_axis_at_sensor_edge is True
        self.run(expo_time, nproj)

    def __info__(self):
        info = """\n
Usage: absct(expo_time, nproj)
       absct(expo_time, nproj, parameter1=value1, parameter2=value2, ...)

Optionnal Parameters:
     rot_center : hry position when sample centerd
     flat_pos : hry position to record flat-field images
     rot_axis_at_sensor_edge :
"""
        return info

    def run(self, expo_time, nproj, **kwargs):
        # --- set optionnal parameters
        if len(kwargs):
            self.pars.from_dict(kwargs)

        # --- check detector
        dets = [ name.split(":")[0] for name in ACTIVE_MG.enabled if name.endswith(":image") ]
        if not len(dets):
            raise RuntimeError("absct: No active camera !! Check ACTIVE_MG.")
        if len(dets) > 1:
            raise RuntimeError("absct: More than one camera active !! Check ACTIVE_MG.")
        if dets[0] not in self.__detectors:
            raise RuntimeError(f"absct: Active camera should be one of {self.__detectors}")
        
        # --- calculate parameters
        pars = self.pars
        omega_step = 180. / nproj
        omega_range = 180. * (1+int(pars.rot_axis_at_sensor_edge)) + (pars.n_over_scan * omega_step)
        npoints = int(omega_range / omega_step)

        # --- prepare fscan parameters
        fscan2d.pars.slow_motor = hry
        fscan2d.pars.slow_start_pos = pars.rot_center
        fscan2d.pars.slow_step_size = 0.1
        fscan2d.pars.slow_npoints = 1

        fscan2d.pars.fast_motor = hrrz
        fscan2d.pars.fast_start_pos = pars.omega_start
        fscan2d.pars.fast_step_size = omega_step
        fscan2d.pars.fast_step_time = expo_time + pars.readout_time
        fscan2d.pars.fast_npoints = npoints
        fscan2d.pars.acq_time = expo_time
      
        fscan2d.pars.display_flag = False

        # --- tomo sequence
        print("absct >> Opening EH3 shutter ...")
        try:
            sheh3.open()
        except RuntimeError:
            print("!! Failed to OPEN EH3 SHUTTER !!")
            print(sheh3.__info__())

        print("absct >> TOMO scan ...")
        fsh.move(1.)
        fscan2d.run()

        print("absct >> FLAT scan ...")
        fscan2d.pars.slow_start_pos = pars.flat_pos
        fscan2d.pars.fast_npoints = pars.nflat
        fscan2d.run()

        print("absct >> DARK scan ...")
        fsh.move(0.)
        fscan2d.pars.slow_start_pos = pars.rot_center
        fscan2d.pars.fast_npoints = pars.ndark
        fscan2d.run()

        print("absct >> Closing EH3 shutter ...")
        try:
            sheh3.close()
        except RuntimeError:
            pass

absct = ABSCTClass()


import tabulate
import numpy
from fscan.fscantools import FScanParamBase, FScanParamStruct

class FastShutterOpened:
    def __init__(self, sh):
        self.__sh = sh

    def __enter__(self):
        self.__sh.move(1.)

    def __exit__(self, *args):
        self.__sh.move(0.)


class TensorXRDCTClass(object):
    def __init__(self):
        self.__detectors = ("pilatus", )
        default_pars = dict(
            readout_time = 0.00095,
            chi_start = 0.,
            chi_end = 40.,
            chi_n = 9,
            y_center = -100.,
            n0 = 60.,
            n0_tilt = 14.,
        )
        self.pars = FScanParamBase("tensortxrdct", default_pars)
        
    def __call__(self, y_scan_range, y_step, z_start, z_end, z_step, expo_time, **kwargs):
        self.run(y_scan_range, y_step, z_start, z_end, z_step, expo_time, **kwargs)

    def __info__(self):
        info = """\n
Usage: tensorxrdct(y_scan_range, y_step, z_start, z_end, z_step, expo_time)

To check chi/phi angles computed:
    tensorxrdct.check(y_scan_range, y_step, z_start, z_end, z_step, expo_time)

To restart a failed scan:
    tensorxrdct.resume() -> restart scan from last fscan2d finished
    tensorxrdct.resume(chi_value) -> restart from the given chi position, performing all phi
    tensorxrdct.resume(chi_value, phi_value) -> restart from the given chi and phi positions
"""
        return info

    def check(self, y_scan_range, y_step, z_start, z_end, z_step, expo_time, **kwargs):
        self._validate_params(y_scan_range, y_step, z_start, z_end, z_step, expo_time, **kwargs)
        self._print_params()

    def run(self, y_scan_range, y_step, z_start, z_end, z_step, expo_time, **kwargs):
        self._validate_params(y_scan_range, y_step, z_start, z_end, z_step, expo_time, **kwargs)
        self._run_from_index(0,0)

    def resume(self, chi_value=None, phi_value=None):
        pars = self.inpars

        if phi_value is not None:
            if chi_value is None:
                raise ValueError("Specify chi_value if you want to set phi_value")

        if chi_value is not None:
            chi_index = numpy.where(abs(pars.chi_pos-chi_value)<.001)
            if len(chi_index) != 1:
                raise ValueError(f"Cannot find CHI index. Values can be:\n{pars.chi_pos}")
            chi_index = int(chi_index[0])
            if phi_value is not None:
                phi_index = numpy.where(abs(pars.phi_pos[chi_index]-phi_value)<.001)[0]
                if len(phi_index) != 1:
                    raise ValueError(f"Cannot find PHI index. Values can be:\n{pars.phi_pos[chi_index]}")
                phi_index = int(phi_index[0])
            else:
                phi_index = 0
        else:
            chi_index = pars.last_chi_index
            phi_index = pars.last_phi_index

        if chi_index < 0 or chi_index >= self.inpars.chi_n:
            raise ValueError(f"Invalid resume chi index. Should be in [0..{pars.chi_n-1}]")
        if phi_index < 0 or phi_index >= len(pars.phi_pos[chi_index]):
            raise ValueError(f"Invalid resume phi index. Should be in [0..{len(pars.phi_pos[chi_index])-1}]")

        self._run_from_index(chi_index, phi_index)


    def _run_from_index(self, chi_index=0, phi_index=0):
        pars = self.inpars
        pars.last_chi_index = 0
        pars.last_phi_index = 0
        
        # --- check detector
        dets = [ name.split(":")[0] for name in ACTIVE_MG.enabled if name.endswith(":image") ]
        if not len(dets):
            raise RuntimeError("tensorxrdct: No active camera !! Check ACTIVE_MG.")
        if len(dets) > 1:
            raise RuntimeError("tensorxrdct: More than one camera active !! Check ACTIVE_MG.")
        if dets[0] not in self.__detectors:
            raise RuntimeError(f"tensorxrdct: Active camera should be one of {self.__detectors}")
        
        # --- open shutters
        print("tensorxrdct >> Opening EH3 shutter ...")
        try:
            sheh3.open()
        except RuntimeError:
            print("!! Failed to OPEN EH3 SHUTTER !!")
            print(sheh3.__info__())

        # --- acquisition loop
        pars = self.inpars
        pars.last_chi_index = chi_index
        pars.last_phi_index = phi_index

        with FastShutterOpened(fsh):
            for idx in range(chi_index, pars.chi_n):
                umv(chi, pars.chi_pos[idx])
                use_phi_index = pars.last_phi_index
                
                for phi_pos_val in pars.phi_pos[idx][use_phi_index:]:
                    print(f"tensorxrdct >> CHI = {pars.chi_pos[idx]:.3f} ; PHI = {phi_pos_val:.3f}")
                    umv(phi, phi_pos_val)
                    fscan2d.run()
                    pars.last_phi_index += 1

                pars.last_chi_index += 1
                pars.last_phi_index = 0

    def _validate_params(self, y_scan_range, y_step, z_start, z_end, z_step, expo_time, **kwargs):
        # --- set optionnal parameters
        if len(kwargs):
            self.pars.from_dict(kwargs)

        # --- init internal parameters struct
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars
        pars.y_scan_range = y_scan_range
        pars.y_step = y_step
        pars.z_start = z_start
        pars.z_end = z_end
        pars.z_step = z_step
        pars.expo_time = expo_time
        
        # --- calulcate parameters
        pars.expo_period = pars.expo_time + pars.readout_time
        pars.y_acq_size = pars.y_step * (pars.expo_time / pars.expo_period)
        pars.y_start = pars.y_center - pars.y_scan_range/2. - pars.y_acq_size/2.
        pars.y_npts = int(pars.y_scan_range / pars.y_step) + 1

        pars.z_npts = int(abs(pars.z_end-pars.z_start) / pars.z_step) + 1
        if (pars.z_start > pars.z_end):
            if pars.z_step > 0:
                pars.z_step *= -1.

        pars.phi_max = numpy.zeros((pars.chi_n,), numpy.float)
        pars.phi_step = numpy.zeros((pars.chi_n,), numpy.float)
        pars.phi_start = numpy.zeros((pars.chi_n,), numpy.float)

        pars.chi_pos = numpy.linspace(pars.chi_start, pars.chi_end, pars.chi_n)
        pars.chi_step = pars.chi_pos[1]-pars.chi_pos[0]

        pars.phi_max[0] = 180.
        pars.phi_max[1:] = 360.
        
        pars.phi_step[0] = 180. / int(pars.n0 * numpy.cos(numpy.radians(pars.chi_pos[0])))
        pars.phi_step[1:] = 180. / (pars.n0_tilt * numpy.cos(numpy.radians(pars.chi_pos[1:]))).astype(int)

        pars.phi_start[0::2] = 0.
        pars.phi_start[1::2] = pars.phi_step[1::2] / 2.

        pars.phi_n = ((pars.phi_max - pars.phi_start) / pars.phi_step).astype(int)

        pars.phi_pos = list()

        for (chi_pos_val, phi_start_val, phi_max_val, phi_step_val) in \
            zip(pars.chi_pos, pars.phi_start, pars.phi_max, pars.phi_step):
                pars.phi_pos.append(numpy.arange(phi_start_val, phi_max_val, phi_step_val))

        # --- print out
        total_npts = pars.y_npts * pars.z_npts * numpy.sum(pars.phi_n)
        total_time = pars.expo_period * total_npts / 3600.

        print(f"\nTENSORXRDCT parameters :\n")
        print(f"- number of y points = {pars.y_npts}")
        print(f"- y translation step = {pars.y_step} mm")
        print(f"- number of z points = {pars.z_npts}")
        print(f"- z translation step = {pars.z_step} mm")
        print(f"- number of tilt angles = {pars.chi_n}")
        print(f"- tilt step             = {pars.chi_step}")
        print()
        print(f"=> total number of frames = {total_npts}")
        print(f"=> total acquisition time = {total_time:.2f} hours")
        print()

        # --- prepare fscan2d
        print(f"FSCAN2D parameters")
        fscan2d.pars.slow_motor      = hrz
        fscan2d.pars.slow_start_pos  = pars.z_start
        fscan2d.pars.slow_step_size  = pars.z_step
        fscan2d.pars.slow_npoints    = pars.z_npts
        fscan2d.pars.fast_motor      = hry
        fscan2d.pars.fast_start_pos  = pars.y_start
        fscan2d.pars.fast_step_size  = pars.y_step
        fscan2d.pars.fast_step_time  = pars.expo_period
        fscan2d.pars.fast_npoints    = pars.y_npts
        fscan2d.pars.acq_time        = pars.expo_time
        fscan2d.pars.display_flag    = False
        fscan2d.show()

    def _print_params(self):
        pars = self.inpars
        for idx in range(pars.chi_n):
            print(f"CHI = {pars.chi_pos[idx]:.3f}")
            print(f"{len(pars.phi_pos[idx])} fscan2d performed at PHI positions :\n{pars.phi_pos[idx]}")
            print()


tensorxrdct = TensorXRDCTClass()


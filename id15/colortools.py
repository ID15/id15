
class ColorTags:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   REVERSE = '\033[7m'
   BLINK = '\033[5m'
   END = '\033[0m'

def __color_message(tag, msg):
    return "{0}{1}{2}".format(tag, msg, ColorTags.END)

def PURPLE(msg):
   return __color_message(ColorTags.PURPLE, msg)
def CYAN(msg):
   return __color_message(ColorTags.CYAN, msg)
def DARKCYAN(msg):
   return __color_message(ColorTags.DARKCYAN, msg)
def BLUE(msg):
   return __color_message(ColorTags.BLUE, msg)
def GREEN(msg):
   return __color_message(ColorTags.GREEN, msg)
def YELLOW(msg):
   return __color_message(ColorTags.YELLOW, msg)
def RED(msg):
   return __color_message(ColorTags.RED, msg)
def UNDERLINE(msg):
   return __color_message(ColorTags.UNDERLINE, msg)
def BOLD(msg):
   return __color_message(ColorTags.BOLD, msg)
def REVERSE(msg):
   return __color_message(ColorTags.REVERSE, msg)
def BLINK(msg):
   return __color_message(ColorTags.BLINK, msg)

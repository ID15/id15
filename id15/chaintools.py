
#from bliss.common.measurement import BaseCounter
#from bliss.scanning.toolbox import _get_object_from_name, _get_counters_from_object

from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.acquisition.counter import SamplingCounterAcquisitionSlave
from bliss.scanning.acquisition.ct2 import CT2AcquisitionMaster
from bliss.scanning.acquisition.timer import SoftwareTimerMaster

MappingTypes = {
    "lima": LimaAcquisitionMaster,
    "ct2": CT2AcquisitionMaster,
    "sampling": SamplingCounterAcquisitionSlave,
}


def get_counters_from_mg(mg):
    counters, missing = [], []
    for name in mg.enabled:
        try:
            obj = _get_object_from_name(name)
        except AttributeError:
            missing.append(name)
        else:
            # Prevent groups from pointing to other groups
            counters += _get_counters_from_object(obj, recursive=False)
    if missing:
        raise AttributeError(*missing)

    # remove duplicates
    counter_dict = {get_counter_name(counter): counter for counter in counters}

    # sort counters
    counters = [counter for name, counter in sorted(counter_dict.items())]

    return counters


def get_counter_name(counter):
#    if not isinstance(counter, BaseCounter):
#        return counter.name
    return counter.fullname


class MGMappingMasterItem(object):
    def __init__(self, device):
        self.name = device.name
        try:
            self.acq_class = device.get_master_device_class()
        except:
            self.acq_class = None
        self.counters = list()
        self.slaves = dict()

    def __str__(self):
        s = "[{0}] : \n- acq_class = {1}\n- counters = {2}\n".format(
            self.name, repr(self.acq_class), list(cnt.name for cnt in self.counters)
        )
        if len(self.slaves):
            s += "- slaves :\n"
            for slave in self.slaves.values():
                for line in str(slave).splitlines():
                    s += f"\t{line}\n"
        return s

    def add_counter(self, counter):
        self.counters.append(counter)


class MGMappingItem(object):
    def __init__(self, device):
        self.name = device.name
        self.acq_class = None
        self.counters = list()

    def __str__(self):
        s = "[{0}] : \n- acq_class = {1}\n- counters = {2}\n".format(
            self.name, repr(self.acq_class), list(cnt.name for cnt in self.counters)
        )
        return s

    def add_counter(self, counter):
        self.counters.append(counter)
        if self.acq_class is None:
            try:
                self.acq_class = counter.get_acquisition_device_class()
            except:
                self.acq_class = counter._acquisition_device_class
            finally:
                pass


class MGMapping(object):
    def __init__(self, mg):
        self.mapping_dict = self.__parse(mg)
        self.done_list = list()

    def __parse(self, mg):
        mapping_dict = {}

        def add_master(master_controller):
            if master_controller is None:
                return None
            if master_controller not in mapping_dict:
                mapping_dict[master_controller] = MGMappingMasterItem(master_controller)
            return mapping_dict[master_controller]

        def add_device(device_controller, master_controller):
            if device_controller is None:
                return None
            master = mapping_dict.get(master_controller, None)
            if master is None:
                if device_controller not in mapping_dict:
                    mapping_dict[device_controller] = MGMappingItem(device_controller)
                return mapping_dict[device_controller]
            else:
                if device_controller not in master.slaves:
                    master.slaves[device_controller] = MGMappingItem(device_controller)
                return master.slaves[device_controller]

        # --- get counters from measurement group
        counters = get_counters_from_mg(mg)
        if not counters:
            return dict()

        for counter in counters:
            master_controller = counter.master_controller
            master = add_master(master_controller)
            device_controller = counter.controller
            device = add_device(device_controller, master_controller)

            if device_controller:
                device.add_counter(counter)
            elif master_controller:
                master.add_counter(counter)

        return mapping_dict

    def pprint(self, mapdict=None):
        if mapdict is None:
            mapdict = self.mapping_dict
        for (device, item) in mapdict.items():
            print(item)

    def get_by_type(self, acq_type):
        acq_class = MappingTypes.get(acq_type, None)
        if acq_class is None:
            raise ValueError(
                "Unknown acquisition type. Should be one of {0}.".format(
                    list(MappingTypes.keys())
                )
            )
        obj_dict = dict()
        for (obj, item) in self.mapping_dict.items():
            if item.acq_class == acq_class:
                obj_dict[obj] = item
        return obj_dict

    def get_by_names(self, *names):
        obj_dict = dict()
        for (obj, item) in self.mapping_dict.items():
            if obj.name in names:
                obj_dict[obj] = item
        return obj_dict

    def get_by_type_and_names(self, acq_type, names):
        obj_dict = self.get_by_type(acq_type)
        if len(names):
            name_dict = dict()
            for (obj, item) in obj_dict.items():
                if obj.name in names:
                    name_dict[obj] = item
            return name_dict
        return obj_dict

    def get_not_done_list(self, *acq_types):
        if len(acq_types):
            obj_dict = dict()
            for acq in acq_types:
                obj_dict.update(self.get_by_type(acq))
        else:
            obj_dict = self.mapping_dict
        res_list = list()
        for obj in obj_dict.keys():
            if obj.name not in self.done_list:
                res_list.append(obj)
        return res_list

    def get_done_list(self, *acq_types):
        if len(acq_types):
            obj_dict = dict()
            for acq in acq_types:
                obj_dict.update(self.get_by_type(acq))
        else:
            obj_dict = self.mapping_dict
        res_list = list()
        for obj in obj_dict.keys():
            if obj.name in self.done_list:
                res_list.append(obj)
        return res_list
        
    def has_name(self, name):
        for obj in self.mapping_dict.keys():
            if obj.name == name:
                return True
        return False

    def has_type(self, acq_type):
        acq_class = MappingTypes.get(acq_type, None)
        if acq_class is None:
            raise ValueError(
                "Unknown acquisition type. Should be one of {0}.".format(
                    list(MappingTypes.keys())
                )
            )
        for item in self.mapping_dict.values():
            if item.acq_class == acq_class:
                return True
        return False

    def setup_ct2(self, chain, master, ct2_pars, *names):
        obj_dict = self.get_by_type_and_names("ct2", names)
        for (master_obj, master_item) in obj_dict.items():
            if master_obj.name in self.done_list:
                continue
            ct2_master = master_item.acq_class(master_obj, **ct2_pars)
            if master is not None:
                chain.add(master, ct2_master)
            else:
                chain.add(ct2_master)
            for (slave_obj, slave_item) in master_item.slaves.items():
                ct2_slave = slave_item.acq_class(
                    *slave_item.counters,
                    npoints=ct2_pars["npoints"],
                    count_time=ct2_pars.get("acq_expo_time", 0),
                )
                chain.add(ct2_master, ct2_slave)
            self.done_list.append(master_obj.name)

    def setup_lima(self, chain, master, lima_pars, *names):
        obj_dict = self.get_by_type_and_names("lima", names)
        for (master_obj, master_item) in obj_dict.items():
            if master_obj.name in self.done_list:
                continue
            lima_pars["saving_statistics_history"] = lima_pars["acq_nb_frames"]
            if master_obj.name == "pilatus":
                lima_pars["saving_format"] = "CBF"
                lima_pars["saving_suffix"] = ".cbf"
                lima_pars["saving_max_writing_task"] = 4
            lima_master = master_item.acq_class(master_obj, **lima_pars)
            for cnt in master_item.counters:
                lima_master.add_counter(cnt)
            if master is not None:
                chain.add(master, lima_master)
            else:
                chain.add(lima_master)
            for (slave_obj, slave_item) in master_item.slaves.items():
                lima_slave = slave_item.acq_class(
                    *slave_item.counters,
                    npoints=lima_pars["acq_nb_frames"],
                    count_time=lima_pars["acq_expo_time"],
                )
                chain.add(lima_master, lima_slave)
            self.done_list.append(master_obj.name)

    def setup_sampling(self, chain, count_time, *names):
        obj_dict = self.get_by_type_and_names("sampling", names)
        if not len(obj_dict):
            return
        acqpars = {"count_time": count_time, "npoints": 0}
        master = SoftwareTimerMaster(
            count_time, npoints=0, sleep_time=0, name="sampling_timer"
        )
        for (obj, item) in obj_dict.items():
            if obj.name not in self.done_list:
                acqdev = item.acq_class(*item.counters, **acqpars)
                chain.add(master, acqdev)
                self.done_list.append(obj.name)

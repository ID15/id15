# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 14:49:06 2020

@author: opid15
"""

def tfguess(energy=None):
    Al2Be = 1.5682766
    if energy is None:
        energy=llmono.energy_now()
    
    _tfguess_v(energy)
    _tfguess_h(energy)
        
        
def _tfguess_v(energy):

	tot=1.9436 - energy*0.106599 + energy*energy*0.009704
	print(f'Total equivalent (Be100) lenses needed vertically: {tot:.1f}')
	print('Try using axes ')
	if tot>50.751:
		print('10 ', end='')
		tot=tot-50.752
	if tot>25.375:
		printf('9 ', end='')
		tot=tot-25.375
     for n in range(4,0,-1):
         if tot > np.power(2,n):
             print(f'{n+4:.1d}', end='')
             tot=tot-np.power(2,n)
	if tot > 0.4:
		print('3 ', end='')
		tot=tot-0.5
	if tot > 0.1:
		print('2 ', end='')
		tot=tot-0.2

     print()
       
def _tfguess_v(energy):

	tot=-34.151 - energy*0.99867 + energy*energy*0.0065732
	print(f'Total equivalent (Be100) lenses needed horizontally: {tot:.1f}')
	print('Try using axes ')
	if tot>63.44:
		print('10 ', end='')
		tot=tot-63.44
	if tot>31.72:
		printf('9 ', end='')
		tot=tot-31.72
     for n in range(4,0,-1):
         if tot > np.power(2,n):
             print(f'{n+4:.1d}', end='')
             tot=tot-np.power(2,n)
	if tot > 0.4:
		print('3 ', end='')
		tot=tot-0.5
	if tot > 0.1:
		print('2 ', end='')
		tot=tot-0.2
      print()

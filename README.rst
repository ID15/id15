============
ID15 project
============

[![build status](https://gitlab.esrf.fr/ID15/id15/badges/master/build.svg)](http://ID15.gitlab-pages.esrf.fr/ID15)
[![coverage report](https://gitlab.esrf.fr/ID15/id15/badges/master/coverage.svg)](http://ID15.gitlab-pages.esrf.fr/id15/htmlcov)

ID15 bliss software

Latest documentation from master can be found [here](http://ID15.gitlab-pages.esrf.fr/id15)
